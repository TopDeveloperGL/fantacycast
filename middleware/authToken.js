const jwt = require("jsonwebtoken");
const keys = require("../config/keys");
const { NOTFOUND, SERVERERROR } = require("../constants/StatusCode");
const { NOTFOUNDMSG, SERVERERRORMSG } = require("../constants/StatusMessage");
const User = require("../models/User.model");

const userToken = (req, res, next) => {
  let token = req.headers["x-auth-token"];
  console.log(token);
  if (!token) {
    return res.status(403).send({
      message: "No token provided!",
    });
  }

  jwt.verify(token, process.env.SECRET_OR_KEY, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        message: "Unauthorized!",
      });
    }
    User.findById(decoded.id)
      .then((user) => {
        if (!user) return res.status(NOTFOUND).json({ errors: NOTFOUNDMSG });
        req.user = user;
        console.log("middleware success");
        return next();
      })
      .catch((err) => {
        return res.status(SERVERERROR).json({ message: SERVERERRORMSG });
      });
  });
};

const authenticate = async (req, res, next) => {
  const token = req.headers["x-auth-token"];
  console.log(token);
  if (!token) {
    return res.status(403).json({ message: "No token provided." });
  }

  try {
    const decoded = jwt.verify(token, process.env.SECRET_KEY);
    console.log(decoded);
    // Use lean() to get plain JavaScript object instead of Mongoose Document
    const user = await User.findById(decoded.id).lean();

    if (!user) {
      return res.status(404).json({ message: "User not found." });
    }

    req.user = user;
    console.log("Auth middleware success.");
    next();
  } catch (err) {
    if (err instanceof jwt.TokenExpiredError) {
      return res.status(401).json({ message: "Token expired." });
    } else if (err instanceof jwt.JsonWebTokenError) {
      return res.status(401).json({ message: "Invalid token." });
    } else {
      return res.status(500).json({ message: "An unknown error occurred." });
    }
  }
};

const adminToken = (req, res, next) => {
  let token = req.headers["x-auth-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!",
    });
  }

  jwt.verify(token, process.env.SECRET_OR_KEY, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!",
      });
    }

    User.findOne({ _id: decoded.id, isAdmin: decoded.isAdmin })
      .then((user) => {
        if (!user) return res.status(NOTFOUND).json({ errors: NOTFOUNDMSG });
        req.user = user;
        console.log("middleware success");
        return next();
      })
      .catch((err) => {
        return res.status(SERVERERROR).json({ message: SERVERERRORMSG });
      });
  });
};

module.exports = {
  userToken,
  adminToken,
  authenticate,
};
