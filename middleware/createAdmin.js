const bcrypt = require("bcryptjs");

const User = require("../models/User.model");

const adminInfo = require("../config/adminInfo");
const { NOTFOUND, SERVERERROR } = require("../constants/StatusCode");
const { NOTFOUNDMSG, SERVERERRORMSG } = require("../constants/StatusMessage");

const createAdmin = async (req, res, next) => {
  let { email, password, is_admin } = adminInfo;

  await User.findOne({ email: email, is_admin: is_admin })
    .then(async (user) => {
      if (!user) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        password = hash;
        const real = {
          email: email,
          password: password ? password : "",
          is_admin: is_admin,
        };
        const result = await User.create(real);
      }
    })
    .catch((err) => {
      return res.status(SERVERERROR).json({ message: SERVERERRORMSG });
    });

  return next();
};

module.exports = { createAdmin };
