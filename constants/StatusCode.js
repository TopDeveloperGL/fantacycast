module.exports = {
  Success: 200,
  Created: 201,
  NoContent: 204,
  NotFound: 404,
  BadRequest: 400,
  ServerError: 500,
  Conflict: 409,
  Invalid: 401,
  EMAILORPASSWORDINVAID: 406,
  EXIST: 403,
};
