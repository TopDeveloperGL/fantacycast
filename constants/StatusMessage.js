module.exports = {
  ConflictMsg: "Already exists",
  CreatedMsg: "Registered successfully",
  NotFoundMsg: "Not found",
  ServerErrorMsg: "Internal server error",
  InvalidMsg: "Invalid email or password",
  SUCCESSMsg: "Success",
  EMAILORPASSWORDINVAIDMsg: "Email or password invalid",
  NOTEQUALPASSWORD: "Previous passwords do not match",
  EXISTMSG: "Email already exists",
};
