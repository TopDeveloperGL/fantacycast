const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Draft = new Schema({
  type: {
    type: String,
  },
  status: {
    type: String,
  },
  start_time: {
    type: Date,
  },
  sport: {
    type: String,
  },
  slot_to_roster_id: {
    type: Object,
  },
  settings: {
    type: Object,
  },
  season_type: {
    type: String,
  },
  season: {
    type: String,
  },
  metadata: {
    type: Object,
  },
  league_id: {
    type: String,
  },
  last_picked: {
    type: Date,
  },
  last_message_time: {
    type: Date,
  },
  last_message_id: {
    type: String,
  },
  draft_order: {
    type: Object,
  },
  draft_id: {
    type: String,
  },
  creators: {
    type: Array,
  },
  created: {
    type: Date,
  },
});

module.exports = mongoose.model("drafts", Draft);
