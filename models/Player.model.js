const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Player = new Schema({
  player_id: {
    type: String,
  },
  status: {
    type: String,
  },
  swish_id: {
    type: Number,
  },
  stats_id: {
    type: Number,
  },
  hashtag: {
    type: String,
  },
  rotoworld_id: {
    type: Number,
  },
  birth_date: {
    type: String,
  },
  depth_chart_position: {
    type: String,
  },
  position: {
    type: String,
  },
  search_full_name: {
    type: String,
  },
  search_rank: {
    type: Number,
  },
  last_name: {
    type: String,
  },
  college: {
    type: String,
  },
  gsis_id: {
    type: String,
  },
  years_exp: {
    type: Number,
  },
  first_name: {
    type: String,
  },
  birth_city: {
    type: String,
  },
  injury_status: {
    type: String,
  },
  team: {
    type: String,
  },
  injury_notes: {
    type: String,
  },
  number: {
    type: Number,
  },
  injury_body_part: {
    type: String,
  },
  sport: {
    type: String,
  },
  practice_participation: {
    type: String,
  },
  birth_country: {
    type: String,
  },
  age: {
    type: Number,
  },
  weight: {
    type: String,
  },
  search_first_name: {
    type: String,
  },
  fantasy_positions: {
    type: Array,
  },
  rotowire_id: {
    type: Number,
  },
  metadata: {
    type: Object,
  },
  full_name: {
    type: String,
  },
  injury_start_date: {
    type: String,
  },
  sportradar_id: {
    type: String,
  },
  practice_description: {
    type: String,
  },
  high_school: {
    type: String,
  },
  birth_state: {
    type: String,
  },
  search_last_name: {
    type: String,
  },
  depth_chart_order: {
    type: Number,
  },
  active: {
    type: Boolean,
  },
  espn_id: {
    type: Number,
  },
  news_updated: {
    type: Number,
  },
  fantasy_data_id: {
    type: Number,
  },
  height: {
    type: String,
  },
  yahoo_id: {
    type: Number,
  },
  pandascore_id: {
    type: String,
  },
});

module.exports = mongoose.model("players", Player);
