const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const League_User = new Schema({
  user_id: {
    type: String,
  },
  settings: {
    type: Object,
  },
  metadata: {
    type: Object,
  },
  league_id: {
    type: String,
  },
  is_owner: {
    type: Boolean,
  },
  is_bot: {
    type: Boolean,
  },
  display_name: {
    type: String,
  },
  avatar: {
    type: String,
  },
});

module.exports = mongoose.model("league_users", League_User);
