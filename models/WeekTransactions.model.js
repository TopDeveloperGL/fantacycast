const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const WeekTransactions = new Schema({
  league_id: {
    type: String,
  },
  week: {
    type: Number,
  },
  transactions: {
    type: Array,
  },
});

module.exports = mongoose.model("league_transactions", WeekTransactions);
