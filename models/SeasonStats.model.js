const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SeasonStats = new Schema({
  season: {
    type: String,
  },
  season_type: {
    type: String,
  },
  stats: {
    type: Object,
  },
});

module.exports = mongoose.model("player_season_stats", SeasonStats);
