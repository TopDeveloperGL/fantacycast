const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Schedule = new Schema({
  user_id: {
    type: ObjectId,
  },
  league_id: {
    type: String,
  },
  report_schedule: {
    type: Object,
  },
  report_voice: {
    type: Object,
  },
});

module.exports = mongoose.model("schedules", Schedule);
