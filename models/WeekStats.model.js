const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const WeekStats = new Schema({
  season: {
    type: String,
  },
  season_type: {
    type: String,
  },
  week: {
    type: Number,
  },
  stats: {
    type: Object,
  },
});

module.exports = mongoose.model("player_week_stats", WeekStats);
