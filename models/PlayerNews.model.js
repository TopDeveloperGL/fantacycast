const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Player_News = new Schema({
  id: {
    type: Number,
  },
  title: {
    type: String,
  },
  url: {
    type: String,
  },
  pubDate: {
    type: Date,
  },
  author: {
    type: String,
  },
  categories: {
    type: Array,
  },
  source: {
    type: Object,
  },
  players: {
    type: Array,
  },
  content: {
    type: String,
  },
});

module.exports = mongoose.model("player_news", Player_News);
