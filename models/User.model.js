const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const User = new Schema({
  sleeper_id: {
    type: String,
  },
  name: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  is_admin: {
    type: Number,
    default: 0,
  },
  is_active: {
    type: Number,
    default: 0,
  },
  user_role: {
    type: Number,
    default: 1,
  },
});

module.exports = mongoose.model("users", User);
