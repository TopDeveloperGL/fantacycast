const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const League = new Schema({
  total_rosters: {
    type: Number,
  },
  status: {
    type: String,
  },
  sport: {
    type: String,
  },
  shard: {
    type: Number,
  },
  settings: {
    type: Object,
  },
  season_type: {
    type: String,
  },
  season: {
    type: String,
  },
  scoring_settings: {
    type: Object,
  },
  roster_positions: {
    type: Object,
  },
  previous_league_id: {
    type: String,
  },
  name: {
    type: String,
  },
  metadata: {
    type: Object,
  },
  loser_bracket_id: {
    type: String,
  },
  league_id: {
    type: String,
  },
  last_read_id: {
    type: String,
  },
  last_pinned_message_id: {
    type: String,
  },
  last_message_time: {
    type: String,
  },
  last_message_text_map: {
    type: String,
  },
  last_message_id: {
    type: String,
  },
  last_message_attachment: {
    type: String,
  },
  last_author_is_bot: {
    type: Boolean,
  },
  last_author_id: {
    type: String,
  },
  last_author_display_name: {
    type: String,
  },
  last_author_avatar: {
    type: String,
  },
  group_id: {
    type: String,
  },
  draft_id: {
    type: String,
  },
  company_id: {
    type: String,
  },
  bracket_id: {
    type: String,
  },
  avatar: {
    type: String,
  },
  start_date: {
    type: Date,
  },
});

module.exports = mongoose.model("leagues", League);
