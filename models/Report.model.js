const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Report = new Schema({
  name: {
    type: String,
  },
  description: {
    type: String,
  },
  schedule_list: {
    type: Array,
  },
  priority: {
    type: Number,
  },
});

module.exports = mongoose.model("reports", Report);
