const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DraftPicks = new Schema({
  league_id: {
    type: String,
  },
  picks: {
    type: Array,
  },
});

module.exports = mongoose.model("draft_picks", DraftPicks);
