const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const WeekProjections = new Schema({
  season: {
    type: String,
  },
  season_type: {
    type: String,
  },
  week: {
    type: Number,
  },
  projections: {
    type: Object,
  },
});

module.exports = mongoose.model("player_week_projections", WeekProjections);
