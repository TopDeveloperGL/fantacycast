const { ObjectId } = require("mongodb");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ReportLog = new Schema({
  league_id: {
    type: String,
  },
  report_id: {
    type: ObjectId,
  },
  user_id: {
    type: ObjectId,
  },
  content: {
    type: String,
  },
  is_run: {
    type: Boolean,
  },
  is_schedule: {
    type: Boolean,
  },
  run_date: {
    type: Date,
  },
  schedule_date: {
    type: Date,
  },
  recipient: {
    type: Array,
  },
  is_file: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("report_logs", ReportLog);
