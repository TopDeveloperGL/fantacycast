const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SeasonProjections = new Schema({
  season: {
    type: String,
  },
  season_type: {
    type: String,
  },
  projections: {
    type: Object,
  },
});

module.exports = mongoose.model("player_season_projections", SeasonProjections);
