const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const League_Matchups = new Schema({
  league_id: {
    type: String,
  },
  week: {
    type: Number,
  },
  matchups: {
    type: Array,
  },
});

module.exports = mongoose.model("league_matchups", League_Matchups);
