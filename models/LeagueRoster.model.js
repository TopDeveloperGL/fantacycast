const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RosterSettings = {
  wins: {
    type: Number,
  },
  waiver_position: {
    type: Number,
  },
  waiver_budget_used: {
    type: Number,
  },
  total_moves: {
    type: Number,
  },
  ties: {
    type: Number,
  },
  losses: {
    type: Number,
  },
  fpts_decimal: {
    type: Number,
  },
  fpts_against_decimal: {
    type: Number,
  },
  fpts_against: {
    type: Number,
  },
  fpts: {
    type: Number,
  },
};

const League_Roster = new Schema({
  taxi: {
    type: Array,
  },
  starters: {
    type: Array,
  },
  settings: {
    type: Object,
  },
  roster_id: {
    type: Number,
  },
  reserve: {
    type: Array,
  },
  players: {
    type: Array,
  },
  player_map: {
    type: String,
  },
  owner_id: {
    type: String,
  },
  metadata: {
    type: Object,
  },
  league_id: {
    type: String,
  },
  keepers: {
    type: Array,
  },
  co_owners: {
    type: String,
  },
  player_details: {
    type: Array,
  },
  team_name: {
    type: String,
  },
});

module.exports = mongoose.model("league_rosters", League_Roster);
