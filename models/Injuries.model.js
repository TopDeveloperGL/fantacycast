const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Injuries = new Schema({
  season: {
    type: Number,
  },
  season_type: {
    type: Number,
  },
  week: {
    type: Number,
  },
  data: {
    type: Array,
  },
});

module.exports = mongoose.model("injuries", Injuries);
