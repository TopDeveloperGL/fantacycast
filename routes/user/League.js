const express = require("express");
const router = express.Router();
const leagueController = require("../../controllers/leagueController");

router.get("/list", leagueController.getLeagueByUser);

module.exports = router;
