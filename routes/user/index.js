const express = require("express");
const router = express.Router();
const authRouter = require("./Auth");
const reportRouter = require("./Report");
const leagueRouter = require("./League");
const profileRouter = require("./Profile");
const reportLogRouter = require("./ReportLog");
const generateRouter = require("./Generate");
const scheduleRouter = require("./schedule");
const { userToken } = require("../../middleware/authToken");

router.use("/auth", authRouter);

router.use("/report", [userToken], reportRouter);

router.use("/league", [userToken], leagueRouter);

router.use("/profile", [userToken], profileRouter);

router.use("/reportlog", reportLogRouter);

router.use("/generate", [userToken], generateRouter);

router.use("/schedule", [userToken], scheduleRouter);

module.exports = router;
