const express = require("express");
const router = express.Router();
const userController = require("../../controllers/UserController");
// const { userToken } = require("../../middleware/authToken");

router.get("/", userController.getUser);

router.post("/update", userController.updateInfoByID);

module.exports = router;
