const express = require("express");
const router = express.Router();
const reportLogController = require("../../controllers/ReportLogController");
const audioController = require("../../controllers/audioController");
const { userToken } = require("../../middleware/authToken");

router.get("/list", [userToken], reportLogController.getAllByUser);

router.get("/listbyreport", [userToken], reportLogController.getAllByReport);

router.get("/audio/download/:id", audioController.download_audio);

router.post("/audio/generate", [userToken], audioController.generate_audio);

router.get("/voice/list", [userToken], audioController.get_voice_list);

router.get("/send/file", audioController.sendFile);

module.exports = router;
