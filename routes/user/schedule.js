const express = require("express");
const router = express.Router();
const scheduleController = require("../../controllers/scheduleController");

router.post("/get", scheduleController.getOne);

router.post("/update", scheduleController.updateOne);

module.exports = router;
