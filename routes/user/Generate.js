const express = require("express");
const router = express.Router();
const promptController = require("../../controllers/PromptController");
const rankingController = require("../../controllers/rankController");
const matchupController = require("../../controllers/matchupsController");
const transactionController = require("../../controllers/transactionController");
const draftController = require("../../controllers/draftController");
const audioController = require("../../controllers/audioController");
// const { userToken } = require("../../middleware/authToken");

router.post("/matchup/before", promptController.makePreMatchupAnalysis);

router.post("/matchup/after", promptController.makePostMatchupAnalysis);

router.post("/rank", promptController.makePowerRanking);

router.post("/team", promptController.makeTeamAnalysis);

router.post("/transaction", promptController.makeTransactionAnalysis);

router.post("/news", promptController.makeTeamNewsAnalysis);

router.post("/ww", promptController.makeWWAnalysis);

router.post("/draft", draftController.draftByTeam);

router.post("/send/email", audioController.sendEmail);

module.exports = router;
