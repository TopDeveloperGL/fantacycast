const express = require("express");
const router = express.Router();
// const newsController = require("../../controllers/newsController");
const reportController = require("../../controllers/ReportController");
const userController = require("../../controllers/UserController");
const matchupController = require("../../controllers/matchupsController");
const buildController = require("../../controllers/buildController");
// const { userToken } = require("../../middleware/authToken");

// router.post("/news", newsController.generate_prompt);

router.get("/list", reportController.getAllByFilter);

router.get("/main", reportController.getAllWithLog);

router.post("/matchup/list", matchupController.getMatchupByLeague);

router.post("/team/list", matchupController.get_team);

router.post("/week/list", buildController.getWeekData);

module.exports = router;
