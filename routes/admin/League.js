const express = require("express");
const router = express.Router();
const leagueController = require("../../controllers/leagueController");

router.put("/update/:id", leagueController.updateLeague);

module.exports = router;
