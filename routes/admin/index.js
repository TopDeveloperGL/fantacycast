const express = require("express");
const router = express.Router();
const authRouter = require("./Auth");
const reportRouter = require("./Report");
const userRouter = require("./User");
const reportLogRouter = require("./ReportLog");
const promptRouter = require("./Prompt");
const leagueRouter = require("./League");

router.use("/auth", authRouter);
router.use("/user", userRouter);
router.use("/report", reportRouter);
router.use("/reportlog", reportLogRouter);
router.use("/prompt", promptRouter);
router.use("/league", leagueRouter);

module.exports = router;
