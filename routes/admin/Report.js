const express = require("express");
const router = express.Router();

const reportController = require("../../controllers/ReportController");

router.post("/add", reportController.addReport);

router.get("/list", reportController.getAllByFilter);

router.post("/update", reportController.updateReport);

router.post("/update/prio", reportController.updatePrio);

router.delete("/delete/:id", reportController.deleteReport);

module.exports = router;
