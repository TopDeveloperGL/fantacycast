const express = require("express");
const router = express.Router();
const authController = require("../../controllers/authController");
const { createAdmin } = require("../../middleware/createAdmin");
const { adminToken } = require("../../middleware/authToken");

router.post("/login", [createAdmin], authController.loginAdmin);
router.post("/change", [adminToken], authController.change_password);

module.exports = router;
