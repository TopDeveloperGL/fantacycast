const express = require("express");
const router = express.Router();
const userController = require("../../controllers/UserController");

router.get("/list", userController.getAllByFilter);

module.exports = router;
