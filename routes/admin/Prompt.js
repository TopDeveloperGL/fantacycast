const express = require("express");
const router = express.Router();
const {
  getMatchupByLeague,
  get_team,
} = require("../../controllers/matchupsController");
const {
  get_Data,
  get_league_list,
  getWeekData,
} = require("../../controllers/buildController");
const {
  get_voice_list,
  download_audio,
  generate_audio,
} = require("../../controllers/audioController");
// const newsController = require("../../controllers/newsController");
const { getCompletion } = require("../../controllers/completionController");
const {
  makePreMatchupAnalysis,
  makePostMatchupAnalysis,
  makePowerRanking,
  makeTransactionAnalysis,
  makeTeamNewsAnalysis,
} = require("../../controllers/GenerateController");
// const { userToken } = require("../../middleware/authToken");

router.post("/build", get_Data);

router.get("/league/list", get_league_list);

router.post("/week/list", getWeekData);

router.post("/team/list", get_team);

router.post("/matchup/list", getMatchupByLeague);

router.post("/matchup/before", makePreMatchupAnalysis);

router.post("/matchup/after", makePostMatchupAnalysis);

router.post("/rank", makePowerRanking);

router.post("/transaction", makeTransactionAnalysis);

router.post("/news", makeTeamNewsAnalysis);

// router.post("/get_draft", draftController.draftByTeam);

router.post("/completion", getCompletion);

router.get("/audio/download/:id", download_audio);

router.post("/generate_audio", generate_audio);

router.get("/voice_list", get_voice_list);

// router.get("/scraping", buildController.webScraping);

// router.post("/news/obtain", newsController.get_news_data);

// router.post("/news/delete", newsController.delete_news_data);

module.exports = router;
