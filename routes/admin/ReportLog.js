const express = require("express");
const router = express.Router();
const reportLogController = require("../../controllers/ReportLogController");
const audioController = require("../../controllers/audioController");

router.get("/list", reportLogController.getAllByFilter);

router.delete("/delete/:id", reportLogController.deleteReport);

router.get("/audio/download/:id", audioController.sendAudioFile);

module.exports = router;
