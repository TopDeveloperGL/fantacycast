const isEmpty = require("is-empty");
const axios = require("axios");
const cheerio = require("cheerio");
const Player_News = require("../models/PlayerNews.model");
const moment = require("moment");

const convert_roster_team_name = (rosterData, owner_team_name) => {
  let save = [];

  rosterData.forEach((roster) => {
    if (!isEmpty(owner_team_name[roster["owner_id"]])) {
      save.push([roster["roster_id"], owner_team_name[roster["owner_id"]]]);
    } else {
      save.push([roster["roster_id"], ""]);
    }
  });

  let roster_id_to_team_name = Object.fromEntries(save);

  return roster_id_to_team_name;
};

const convert_player_name = (playerData) => {
  let save = [];

  playerData.forEach((player) => {
    if (player["position"] === "DEF") {
      save.push([
        player["player_id"],
        `${player["first_name"]} ${player["last_name"]}`,
      ]);
    } else {
      save.push([player["player_id"], player["full_name"]]);
    }
  });

  let player_id_to_name = Object.fromEntries(save);

  return player_id_to_name;
};

const convert_owner_team_name = (user_data) => {
  let save = [];

  user_data.forEach((user) => {
    if (!isEmpty(user["metadata"]) && !isEmpty(user["metadata"]["team_name"])) {
      save.push([user["user_id"], user["metadata"]["team_name"]]);
    } else {
      save.push([user["user_id"], user["display_name"]]);
    }
  });

  let user_id_to_team_name = Object.fromEntries(save);

  return user_id_to_team_name;
};

const map_users_to_team_name = (users) => {
  let users_dict = {};
  users.forEach((user) => {
    if (!isEmpty(user["metadata"]["team_name"])) {
      users_dict[user["user_id"]] = user["metadata"]["team_name"];
    } else {
      users_dict[user["user_id"]] = user["display_name"];
    }
  });
  return users_dict;
};

const map_players_to_rosters = (player_data, roster_data, user_data) => {
  let save = [];
  user_data.forEach((user) => {
    if (!isEmpty(user["metadata"]) && !isEmpty(user["metadata"]["team_name"])) {
      save.push([user["user_id"], user["metadata"]["team_name"]]);
    } else {
      save.push([user["user_id"], user["display_name"]]);
    }
  });

  let user_id_to_team_name = Object.fromEntries(save);
  let roster_with_players = [];

  roster_data.forEach((roster) => {
    let players = roster["players"];
    let player_details = [];
    players.forEach((player_id) => {
      if (!isEmpty(player_data[player_id])) {
        player_details.push(player_data[player_id]);
      }
    });

    roster["player_details"] = player_details;
    roster["team_name"] = user_id_to_team_name[roster["owner_id"]] || "Unknown";
    roster_with_players.push(roster);
  });

  return roster_with_players;
};

const convert_player_ids_to_names = (data) => {
  data.forEach((team) => {
    let save = [];
    team["player_details"].forEach((player) => {
      if (player["position"] === "DEF") {
        save.push([
          player["player_id"],
          `${player["first_name"]} ${player["last_name"]}`,
        ]);
      } else {
        save.push([player["player_id"], player["full_name"]]);
      }
    });
    let player_id_to_name = Object.fromEntries(save);
    ["starters", "players", "keepers", "reserve"].forEach((key) => {
      if (!isEmpty(team[key])) {
        team[key] = team[key].map((player_id) => {
          if (!isEmpty(player_id_to_name[player_id])) {
            return player_id_to_name[player_id];
          } else {
            return player_id;
          }
        });
      }
    });
  });
  return data;
};

const webScraping = async () => {
  let page = 1;
  let flag = true;
  let playerStats = [];
  let start = new Date();
  let end = new Date();
  end.setDate(end.getDate() - 1);
  while (flag) {
    let url = `https://www.rotoballer.com/player-news/page/${page}?sport=nfl`;
    await axios(url)
      .then((response) => {
        const html = response.data;
        const $ = cheerio.load(html);
        page += 1;
        $("#content").each((index, element) => {
          $(element)
            .find(".newsdeskContentEntry")
            .each((idx, ele) => {
              let date = $(ele).attr("post_time");
              let source = $(ele).find(".newsSource").text();

              $(ele).find(".shareBox").empty();
              $(ele).find(".newsDate").empty();
              $(ele).find(".newsByline").empty();
              $(ele).find(".newsSource").empty();
              let text = $(ele).text();
              if (new Date(date) <= start && new Date(date) >= end)
                playerStats.push({ date: date, text: text, source: source });
              else flag = false;
            });
        });
      })
      .catch(console.error);
  }
  if (!flag) return playerStats;
};

// To extract content in html
const html_to_text = (html) => {
  // let html = `New England Patriots wide receiver <a class="rbPlayer nfl" href="https://www.rotoballer.com/nfl/player/16775/DeVante+Parker" data-id="16775">DeVante Parker</a>'s new contract extension shouldn't limit or enhance the team's pursuit of free-agent wideout <a class="rbPlayer nfl" href="https://www.rotoballer.com/nfl/player/14986/DeAndre+Hopkins" data-id="14986">DeAndre Hopkins</a>, who visited the Patriots on June 15. Parker's extension is unrelated to anything with Hopkins and instead is specific to crafting a deal for Parker that will reward him if he's on the field and productive, and it protects the team more than his prior deal if he isn't productive. New England has reportedly offered a contract to Hopkins, but he's in no rush to sign and is trying to drum up interest from more teams before training camp starts at the end of July. Until other teams show interest in the 31-year-old, though, the Patriots will remain a likely destination for Hopkins, who would immediately become their No. 1 wideout.`;
  let $ = cheerio.load(html);
  let text = $.text();

  return text;
};

// To get player news in rotoballer
const get_player_news = async (end) => {
  let date = new Date(end);
  let startDate = moment(date.setDate(date.getDate() - 3)).format("YYYYMMDD");
  let endDate = moment(new Date(end)).format("YYYYMMDD");

  console.log(startDate, endDate);
  let url = `https://www.rotoballer.com/partner-rssxml?partner=ardent_principles&key=3hR.p.K9Q5CTi.Mj9sQRbY6VsGszQ6YDalCunCZdBGU1AM97CQK1vZ1DIpUk&sport=nfl&format=json&dateStart=${startDate}&dateStop=${endDate}`;
  console.log(url);
  await axios(url)
    .then((res) => Player_News.insertMany(res.data))
    .catch((err) => console.log(err));
};

const clamp = (value, min, max) => {
  return Math.min(Math.max(value, min), max);
};

const get_standings = (rosters, roster_name) => {
  let roster_standings_list = [];
  let rank_list = {};

  rosters.forEach((item) => {
    let wins = item.settings.wins;
    let losses = item.settings.losses;
    let ties = item.settings.ties;
    let points = item.settings.fpts;
    let points_against = item.settings.fpts_against;
    let name = roster_name[item.roster_id];

    roster_standings_list.push({
      roster_id: item.roster_id,
      teamName: name,
      wins: wins,
      losses: losses,
      pointsFor: points,
      pointsAgainst: points_against,
      percent:
        (wins / (wins + losses + ties)) * 0.5 +
        (points / (points + points_against)) * 0.5,
    });
  });

  roster_standings_list.sort((a, b) => b.percent - a.percent);
  roster_standings_list = roster_standings_list.map((item, idx) => {
    return { ...item, rank: idx + 1 };
  });

  return roster_standings_list;
};

const calculateWeeks = (startDate, futureDate) => {
  startDate = new Date(startDate);
  futureDate = new Date(futureDate);
  const oneDay = 24 * 60 * 60 * 1000;
  const diffTime = Math.abs(futureDate - startDate);
  const diffDays = Math.ceil(diffTime / oneDay);
  return Math.floor(diffDays / 7) + 2;
};

const getNextMonday = (dateString) => {
  let date = new Date(dateString);

  let daysUntilNextMonday = (1 + 7 - date.getDay()) % 7;

  if (daysUntilNextMonday === 0) {
    daysUntilNextMonday = 7;
  }

  date.setDate(date.getDate() + daysUntilNextMonday);

  return date.toISOString().split("T")[0];
};

module.exports = {
  convert_roster_team_name,
  convert_player_name,
  convert_owner_team_name,
  map_users_to_team_name,
  map_players_to_rosters,
  convert_player_ids_to_names,
  webScraping,
  html_to_text,
  get_player_news,
  clamp,
  get_standings,
  calculateWeeks,
  getNextMonday,
};
