const Schedule = require("../models/Schedule.model");
const JWT = require("jsonwebtoken");
const {
  ServerError,
  Conflict,
  Created,
  NotFound,
  Success,
  Invalid,
  NoContent,
} = require("../constants/StatusCode");
const {
  ConflictMsg,
  SUCCESSMsg,
  ServerErrorMsg,
  CreatedMsg,
  NotFoundMsg,
  InvalidMsg,
} = require("../constants/StatusMessage");

const getOne = async (req, res) => {
  console.log("get schedule");
  try {
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { id } = decoded;
    const scheduleOne = await Schedule.findOne({
      user_id: id,
      league_id: req.body.league,
    });
    console.log(scheduleOne);
    if (scheduleOne) {
      res.status(Success).json({ schedule: scheduleOne, message: "Success" });
    } else {
      await Schedule.create({ user_id: id, league_id: req.body.league });
      res.status(NoContent).json({
        schedule: {},
        message: "Schedule not found.",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const updateOne = async (req, res) => {
  console.log(req.body);
  try {
    let newData = {
      report_schedule: req.body.report_schedule,
      report_voice: req.body.report_voice,
    };
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { id } = decoded;

    const updatedSchedule = await Schedule.findOneAndUpdate(
      { user_id: id, league_id: req.body.league },
      newData,
      {
        new: true,
      }
    );

    if (updatedSchedule) {
      res
        .status(Success)
        .json({ schedule: updatedSchedule, message: "Update successful" });
    } else {
      res.status(NotFound).json({ message: "Schedule not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

module.exports = {
  getOne,
  updateOne,
};
