const mongoose = require("mongoose");
const JWT = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const User = require("../models/User.model");

const { Success, ServerError, NotFound } = require("../constants/StatusCode");
const { ServerErrorMsg } = require("../constants/StatusMessage");

const getAllByFilter = async (req, res) => {
  const options = req.query;
  console.log(options);
  try {
    // let condition = {
    //   $or: [
    //     { name: { $regex: options?.search } },
    //     { email: { $regex: options?.search } },
    //   ],
    // };

    const count = await User.aggregate([
      { $match: { is_admin: 0 } },
      // { $match: condition },
    ]);

    if (count.length) {
      let userList = await User.aggregate([
        { $match: { is_admin: 0 } },
        // { $match: condition },
        { $skip: (options.page - 1) * options.per_page },
        {
          $limit:
            options.per_page > count.length ? count.length : options.per_page,
        },
      ]);
      res.status(Success).json({
        data: userList,
        page: options.page,
        per_page: options.per_page,
        total: count.length,
        total_pages: count.length / options.per_page + 1,
      });
    } else {
      res.status(Success).json({
        data: [],
        page: options.page,
        per_page: options.per_page,
        total: count.length,
        total_pages: count.length / options.per_page + 1,
      });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error." });
  }
};

const getUser = async (req, res) => {
  try {
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { sleeper_id, id } = decoded;
    console.log("profile", sleeper_id, id);

    const user = await User.aggregate([
      {
        $match: {
          sleeper_id: sleeper_id,
          _id: new mongoose.Types.ObjectId(id),
        },
      },
      {
        $lookup: {
          from: "league_users",
          localField: "sleeper_id",
          foreignField: "display_name",
          as: "sleeper_user",
        },
      },
      { $unwind: "$sleeper_user" },
      {
        $project: {
          _id: 0, // Optional: remove Mongo's default _id field from the result
          sleeperName: "$sleeper_user.display_name",
          sleeperId: 1,
          name: 1,
          email: 1,
          password: 1,
          report_schedule: 1,
          report_voice: 1,
        },
      },
    ]);
    console.log("profile", user);

    if (user.length) {
      res.status(Success).json({ user: user[0] });
    } else {
      res.status(NotFound).json({ user: {}, message: "User not found." });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

// const updateUserByID = async (req, res) => {
//   console.log(req.body);
//   try {
//     let newData = req.body;
//     const token = req.headers["x-auth-token"];
//     const decoded = JWT.decode(token);
//     const { id } = decoded;

//     const updatedUser = await User.findByIdAndUpdate(id, newData, {
//       new: true,
//     });

//     if (updatedUser) {
//       res
//         .status(Success)
//         .json({ user: updatedUser, message: "Update successful" });
//     } else {
//       res.status(NotFound).json({ message: "User not found" });
//     }
//   } catch (error) {
//     console.error(error);
//     res.status(ServerError).json({ message: ServerErrorMsg });
//   }
// };

const updateInfoByID = async (req, res) => {
  console.log(req.body);
  try {
    let newData = req.body;
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { id } = decoded;
    const { password } = newData;

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const updatedUser = await User.findByIdAndUpdate(
      id,
      { ...newData, password: hashedPassword },
      {
        new: true,
      }
    );

    if (updatedUser) {
      res
        .status(Success)
        .json({ user: updatedUser, message: "Update successful" });
    } else {
      res.status(NotFound).json({ message: "User not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

module.exports = {
  getAllByFilter,
  getUser,
  updateInfoByID,
};
