const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_Matchup = require("../models/LeagueMatchups.model");
const League_User = require("../models/LeagueUser.model");
const DraftPicks = require("../models/DraftPicks.model");
const League_Transaction = require("../models/WeekTransactions.model");
const Player_Stats = require("../models/WeekStats.model");
const Player_Projections = require("../models/WeekProjections.model");
const sleeper_package = require("sleeper_fantasy");
const isEmpty = require("is-empty");
const axios = require("axios");
const cheerio = require("cheerio");
const { map_users_to_team_name } = require("../utils/report");
const { default: mongoose } = require("mongoose");

const get_leagues_by_user = async (leagueData, weeks) => {
  const sleeper_instance = new sleeper_package.sleeper();

  const league_ids = [leagueData["league_id"]];
  sleeper_instance.leagues = league_ids;

  await Promise.all(
    weeks.map(async (week) => {
      let matchupData = [];
      let matchup_count = await League_Matchup.count({
        league_id: leagueData["league_id"],
        week: week,
      });
      if (!matchup_count) {
        let fetch_matchups = await sleeper_instance.leagues[
          leagueData["league_id"]
        ].fetch_matchups(week);

        matchupData = fetch_matchups.matchups;
        League_Matchup.create({
          league_id: leagueData["league_id"],
          week: week,
          matchups: matchupData,
        });
      }

      let transactionData = [];
      let transaction_count = await League_Transaction.count({
        league_id: leagueData["league_id"],
        week: week,
      });
      if (!transaction_count) {
        let fetch_transaction = await sleeper_instance.leagues[
          leagueData["league_id"]
        ].fetch_transactions(week);
        transactionData = fetch_transaction.transactions;
        League_Transaction.create({
          league_id: leagueData["league_id"],
          week: week,
          data: transactionData,
        });
      }
    })
  );

  const draft_ids = [leagueData["draft_id"]];
  sleeper_instance.drafts = draft_ids;

  let draftData = [];
  let draft_count = await DraftPicks.count({
    league_id: leagueData["league_id"],
  });
  if (!draft_count) {
    const fetch_draft_picks = await sleeper_instance.drafts[
      leagueData["draft_id"]
    ].picks();
    draftData = fetch_draft_picks._picks;
    DraftPicks.create({
      league_id: leagueData["league_id"],
      picks: draftData,
    });
  }
};

const get_stats_projections = async (leagueData, weeks) => {
  const sleeper_instance = new sleeper_package.sleeper();

  let league_count = await League.count({ league_id: leagueData["league_id"] });
  if (!league_count) {
    await League.create(leagueData);
  }

  await Promise.all(
    weeks.map(async (week) => {
      let statsData = [];
      let stats_count = await Player_Stats.count({
        season: leagueData["season"],
        season_type: leagueData["season_type"],
        week: week,
      });

      let projectionsData = [];
      let projections_count = await Player_Projections.count({
        season: leagueData["season"],
        season_type: leagueData["season_type"],
        week: week,
      });

      if (!stats_count) {
        let fetch_stats = await sleeper_instance.players.fetch_stats(
          leagueData["season_type"],
          leagueData["season"],
          week
        );
        statsData = fetch_stats.stats;
        await Player_Stats.create({
          season: leagueData["season"],
          season_type: leagueData["season_type"],
          week: week,
          stats: statsData,
        });
      }

      if (!projections_count) {
        let fetch_projections =
          await sleeper_instance.players.fetch_projections(
            leagueData["season_type"],
            leagueData["season"],
            week
          );
        projectionsData = fetch_projections.projections;
        await Player_Projections.create({
          season: leagueData["season"],
          season_type: leagueData["season_type"],
          week: week,
          projections: projectionsData,
        });
      }
    })
  );

  let stats_total = await Player_Stats.count({
    season: leagueData["season"],
    season_type: leagueData["season_type"],
    week: 0,
  });

  if (!stats_total) {
    let fetch_total = await sleeper_instance.players.fetch_stats(
      leagueData["season_type"],
      leagueData["season"]
    );
    await Player_Stats.create({
      season: leagueData["season"],
      season_type: leagueData["season_type"],
      week: 0,
      stats: fetch_total.stats,
    });
  }

  let projections_total = await Player_Projections.count({
    season: leagueData["season"],
    season_type: leagueData["season_type"],
    week: 0,
  });

  if (!projections_total) {
    let fetch_total = await sleeper_instance.players.fetch_projections(
      leagueData["season_type"],
      leagueData["season"]
    );
    await Player_Projections.create({
      season: leagueData["season"],
      season_type: leagueData["season_type"],
      week: 0,
      projections: fetch_total.projections,
    });
  }
};

const get_Data = async (req, res) => {
  const sleeper_instance = new sleeper_package.sleeper();

  let playerData = [];
  // let player_count = await Player.count();
  await Player.deleteMany({});
  const fetch_player = await sleeper_instance.players.fetch_all_players();
  let player_value = fetch_player.all_players;
  playerData = Object.keys(player_value).map((key) => {
    return { ...player_value[key] };
  });
  await Player.insertMany(playerData);

  const usernames = [req.body.userName];
  sleeper_instance.users = usernames;
  await Promise.all(sleeper_instance.user_promises);
  const user_leagues = await sleeper_instance.users[
    req.body.userName
  ].fetch_leagues(parseInt(req.body.season));

  // console.log(user_leagues);
  const league_list = user_leagues?.leagues.map((item) => item["league_id"]);

  let weeks = [];
  for (let i = 1; i <= 18; i++) {
    weeks.push(i);
  }

  const league_ids = league_list;
  // const league_ids = [req.body.league];
  sleeper_instance.leagues = league_ids;
  let fetchleagues = await Promise.all(sleeper_instance.league_promises);

  let season = new Date().getFullYear();
  // let leagueData = fetchleagues;

  await Promise.all(
    fetchleagues.map(async (leagueData, idx) => {
      await League.deleteOne({ league_id: leagueData["league_id"] });
      League.create(leagueData);

      let rosterData = [];
      let roster_count = await League_Roster.count({
        league_id: leagueData["league_id"],
      });
      if (!roster_count) {
        const fetch_rosters = await sleeper_instance.leagues[
          leagueData["league_id"]
        ].fetch_rosters();
        rosterData = fetch_rosters.rosters;
        League_Roster.insertMany(rosterData);
      }

      let userData = [];
      let user_count = await League_User.count({
        league_id: leagueData["league_id"],
      });
      if (!user_count) {
        const fetch_users = await sleeper_instance.leagues[
          leagueData["league_id"]
        ].fetch_owners();
        userData = fetch_users.owners;
        League_User.insertMany(userData);
      }

      if (leagueData.season !== season.toString()) {
        await get_stats_projections(leagueData, weeks);

        await get_leagues_by_user(leagueData, weeks);
      }

      // await get_leagues_by_user(leagueData, weeks);
      if (idx === fetchleagues.length - 1) {
        res.send("ok");
      }
    })
  );

  res.send("ok");
  // leagueData = leagueData?.filter(
  //   (item) => !isEmpty(item["previous_league_id"])
  // );

  // for (let i = 0; i < leagueData.length; i++) {
  //   await get_stats_projections(leagueData[i], weeks);
  // }
};

const get_league_list = async (req, res) => {
  const leagueList = await League.aggregate([
    { $project: { _id: 0, league_id: 1, name: 1, season: 1 } },
  ]);
  res.send(leagueList);
};

const getWeekData = async (req, res) => {
  console.log("week", req.body.league);
  const weekList = await League_Matchup.aggregate([
    { $match: { league_id: req.body.league } },
    { $project: { _id: 0, week: 1 } },
    { $sort: { week: 1 } },
  ]);
  res.send(weekList);
};

const getTeamData = async (req, res) => {
  // let teams_list = await Teams.find();
  // let output = teams_list.map((team) => {
  //   return { Key: team.Key, FullName: team.FullName };
  // });
  // res.send(output);
  let userData = [];
  let user_count = await League_User.count({ league_id: req.body.league });
  if (user_count) {
    userData = await League_User.find({ league_id: req.body.league });
  }
  let data = map_users_to_team_name(userData);
  res.send(data);
};

const webScraping = async (req, res) => {
  let page = 1;
  let flag = true;
  let playerStats = [];
  let start = new Date();
  let end = new Date();
  end.setDate(end.getDate() - 3);
  while (flag) {
    let url = `https://www.rotoballer.com/player-news/page/${page}?sport=nfl`;
    await axios(url)
      .then((response) => {
        const html = response.data;
        const $ = cheerio.load(html);
        page += 1;
        $("#content").each((index, element) => {
          $(element)
            .find(".newsdeskContentEntry")
            .each((idx, ele) => {
              let date = $(ele).attr("post_time");
              let source = $(ele).find(".newsSource").text();

              $(ele).find(".shareBox").empty();
              $(ele).find(".newsDate").empty();
              $(ele).find(".newsByline").empty();
              $(ele).find(".newsSource").empty();
              let text = $(ele).text();
              if (new Date(date) <= start && new Date(date) >= end)
                playerStats.push({ date: date, text: text, source: source });
              else flag = false;
            });
        });
      })
      .catch(console.error);
  }
  console.log(playerStats.length);
  if (!flag) res.send(playerStats);
};

module.exports = {
  get_Data,
  get_league_list,
  getWeekData,
  webScraping,
  getTeamData,
};
