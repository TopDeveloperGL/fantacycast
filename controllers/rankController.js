const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_Matchup = require("../models/LeagueMatchups.model");
const League_User = require("../models/LeagueUser.model");
// const Injuries = require("../models/Injuries.model");
const SeasonStats = require("../models/SeasonStats.model");
const SeasonProjections = require("../models/SeasonProjections.model");
const axios = require("axios");
const sleeper_package = require("sleeper_fantasy");
const isEmpty = require("is-empty");
const {
  convert_owner_team_name,
  convert_player_name,
  convert_roster_team_name,
  webScraping,
  clamp,
  get_standings,
} = require("../utils/report");

const map_rosterid_to_ownerid = (rosters) => {
  let result_dict = {};
  rosters.forEach((roster) => {
    let roster_id = roster["roster_id"];
    let owner_id = roster["owner_id"];
    result_dict[roster_id] = owner_id;
  });

  return result_dict;
};

const map_users_to_team_name = (users) => {
  let users_dict = {};
  users.forEach((user) => {
    if (!isEmpty(user["metadata"]["team_name"])) {
      users_dict[user["user_id"]] = user["metadata"]["team_name"];
    } else {
      users_dict[user["user_id"]] = user["display_name"];
    }
  });
  return users_dict;
};

const get_team_score = (stats, starters, score_type) => {
  let total_score = 0;

  starters.forEach((starter) => {
    if (!isEmpty(stats[starter]) && !isEmpty(stats[starter][score_type])) {
      total_score += stats[starter][score_type];
    }
  });

  return total_score;
};

const get_players_score = (week_stats, players) => {
  let player_scores = {};
  let score_type = ["pts_std", "pts_ppr", "pts_half_ppr"];
  players.forEach((player) => {
    player_scores[player] = {};
    score_type.forEach((item) => {
      if (!isEmpty(week_stats[player]) && !isEmpty(week_stats[player][item])) {
        player_scores[player][item] = week_stats[player][item];
      } else player_scores[player][item] = null;
    });
  });

  return player_scores;
};

const get_close_games = (scoreboards, close_num) => {
  let close_games_dict = {};
  for (let key in scoreboards) {
    let team_one_score = scoreboards[key][0][1];
    let team_two_score = scoreboards[key][1][1];

    if (Math.abs(team_one_score - team_two_score) < close_num)
      close_games_dict[key] = scoreboards[key];
  }
  return close_games_dict;
};

const get_stats_text = (team, stats, player_name, score_type) => {
  let players = team["players"];
  let summary = [];
  players.forEach((player) => {
    if (!isEmpty(stats[player])) {
      if (!isEmpty(stats[player][score_type])) {
        summary.push(`${player_name[player]}(${stats[player][score_type]})`);
      }
    }
  });
  return summary.join(", ");
};

const add_space = (basic_len, total_len) => {
  let str = "";
  while (total_len >= basic_len) {
    str += " ";
    basic_len++;
  }
  return str;
};

const get_power_ranking = async (req, res) => {
  console.log(req.body);
  let player_count = await Player.count();
  let playerData = [];
  if (player_count) {
    playerData = await Player.find();
  }
  let leagueData = {};
  let league_count = await League.count({ league_id: req.body.league });
  if (league_count) {
    leagueData = await League.findOne({ league_id: req.body.league });
  }

  let rosterData = [];
  let roster_count = await League_Roster.count({ league_id: req.body.league });
  if (roster_count) {
    rosterData = await League_Roster.find({ league_id: req.body.league });
  }
  let rosterTeam = await League_Roster.find({
    league_id: req.body.league,
  });

  let userData = [];
  let user_count = await League_User.count({ league_id: req.body.league });
  if (user_count) {
    userData = await League_User.find({ league_id: req.body.league });
  }

  let matchupData = [];
  let matchup_count = await League_Matchup.count({
    league_id: req.body.league,
  });
  if (matchup_count) {
    matchupData = await League_Matchup.findOne({
      league_id: req.body.league,
      week: req.body.week,
    });
  }

  let statsData = await Player_Stats.findOne({
    season: leagueData["season"],
    season_type: leagueData["season_type"],
    week: req.body.week,
  });

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_name = convert_roster_team_name(rosterTeam, owner_team_name);

  let players = [];
  playerData.forEach((item) => {
    players.push([item.player_id, item]);
  });

  let standings = get_standings(rosterData, userData);
  let standings_text = "";
  standings_text = standings_text.concat(
    "Team Name" +
      add_space(9, 25) +
      "Wins" +
      add_space(4, 4) +
      "Losses" +
      add_space(6, 6) +
      "Points" +
      add_space(6, 6) +
      "\n"
  );
  standings.forEach((team) => {
    standings_text = standings_text.concat(
      `${team[0]}` +
        add_space(team[0].length, 25) +
        `${team[1]}` +
        add_space(team[1].toString().length, 4) +
        `${team[2]}` +
        add_space(team[2].toString().length, 6) +
        `${team[3]}` +
        add_space(team[3].toString().length, 6) +
        "\n"
    );
  });

  let scoreboards = get_scoreboards(
    rosterData,
    matchupData.matchups,
    userData,
    statsData.stats,
    "pts_std"
  );

  let scoreboards_array = Object.entries(scoreboards);
  let scoreboards_summary = [];
  scoreboards_array.forEach((item) => {
    scoreboards_summary.push(`-Matchup ${item[0]}\n${item[1].join(" | ")}`);
  });
  let scoreboards_text = scoreboards_summary.join("\n");

  let close_games = get_close_games(scoreboards, 5);
  let close_games_array = Object.entries(close_games);
  let close_games_summary = [];
  close_games_array.forEach((item) => {
    close_games_summary.push(`-Matchup ${item[0]}\n${item[1].join(" | ")}`);
  });
  let close_games_text = close_games_summary.length
    ? close_games_summary.join("\n")
    : "Empty\n";

  // let stats_summary = [];
  // matchupData.matchups.forEach((item) =>
  //   stats_summary.push(`Team Name: ${
  //     roster_name[item["roster_id"]]
  //   }\n${get_stats_text(item, statsData.stats, player_name, "pts_std")}
  //   `)
  // );
  let news_summary = [];
  let news_list = [];
  let playerNews = await webScraping();

  let playerNames = [];
  matchupData.matchups.forEach((item) => {
    playerNames = playerNames.concat(
      item["players"].map((player) => {
        return player_name[player];
      })
    );
  });

  playerNews?.forEach((playerNew, idx) => {
    playerNames.forEach((playerName) => {
      if (
        playerNew["text"].search(playerName) !== -1 &&
        !news_list.includes(idx)
      )
        news_list.push(idx);
    });
  });
  news_list?.forEach((item) => {
    news_summary.push(playerNews[item]["text"]);
  });
};

/**
 * Generates power ranking prompt
 */
const generate_prompt = async (req, res) => {
  console.log(req.body);
  const sleeper_instance = new sleeper_package.sleeper();
  let playerData = await Player.find();

  let leagueData = await League.findOne({ league_id: req.body.league });

  let roster_data = await League_Roster.findOne({
    league_id: req.body.league,
    owner_id: req.body.team,
  });

  let rosterTeam = await League_Roster.find({
    league_id: req.body.league,
  });

  let matchup_data = await League_Matchup.find({
    league_id: req.body.league,
  });

  let userData = await League_User.find({
    league_id: req.body.league,
  });

  // let weeks = [];
  // for (let i = 1; i <= 18; i++) {
  //   weeks.push(i);
  // }

  // let injuries_weeks = [];
  // await Promise.all(
  //   weeks?.map(async (item) => {
  //     let injuries_by_week = await Injuries.findOne({
  //       season: leagueData.season,
  //       season_type: 1,
  //       week: item,
  //     });

  //     let injuries_team = injuries_by_week?.data;

  //     if (isEmpty(injuries_by_week)) {
  //       let url = `https://api.sportsdata.io/v3/nfl/stats/json/Injuries/${leagueData.season}/${item}?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //       await axios(url)
  //         .then((injuries) => {
  //           Injuries.create({
  //             season: leagueData.season,
  //             season_type: 1,
  //             week: item,
  //             data: injuries.data,
  //           });
  //           injuries_team = injuries.data;
  //         })
  //         .catch((err) => console.log(err));
  //     }
  //     injuries_weeks = injuries_weeks.concat(injuries_team);
  //   })
  // );

  // console.log(injuries_weeks.length);

  // let teams_list = await Teams.find();

  let stats_by_season = await SeasonStats.findOne({
    season: leagueData.season,
    season_type: leagueData.season_type,
  });

  let stats_data = stats_by_season?.data;

  if (isEmpty(stats_by_season)) {
    let fetchStats = await sleeper_instance.players.fetch_stats(
      leagueData["season_type"],
      leagueData["season"]
    );
    SeasonStats.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      data: fetchStats.stats,
    });
    stats_data = fetchStats.stats;
  }
  // console.log(stats_data);

  let projections_by_season = await SeasonProjections.findOne({
    season: leagueData.season,
    season_type: leagueData.season_type,
  });

  let projections_data = projections_by_season?.data;

  if (isEmpty(projections_by_season)) {
    let fetchProjections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season
    );
    SeasonProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      data: fetchProjections.projections,
    });

    projections_data = fetchProjections.projections;
  }

  // console.log(projections_data);

  // let league_players = await LeaguePlayers.find();

  // if (isEmpty(league_players)) {
  //   let url = `https://api.sportsdata.io/v3/nfl/scores/json/Players?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //   await axios(url)
  //     .then((players) => {
  //       LeaguePlayers.create(players.data);
  //       league_players = players.data;
  //     })
  //     .catch((err) => console.log(err));
  // }

  // console.log(league_players.length);

  let players = [];
  playerData.forEach((item) => {
    players.push([item.player_id, item]);
  });

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_name = convert_roster_team_name(rosterTeam, owner_team_name);

  let standings_data = get_standings(rosterTeam, roster_name);

  let strengthOfSchedule = calculate_strength_of_schedule(
    standings_data,
    roster_data.roster_id,
    get_opponents(matchup_data, roster_data.roster_id)
  );

  let standings = [];
  let roster_info = standings_data.filter(
    (sub_item) => sub_item.roster_id === roster_data.roster_id
  )[0];

  let player_ids = rosterTeam.filter(
    (team) => team.roster_id === roster_data.roster_id
  )[0].players;

  // let injuryReport = [];
  let playerStats = [];
  console.log(player_ids);
  player_ids.forEach((player_id) => {
    // let injury_info = injuries_weeks.filter(
    //   (injury) => injury.PlayerID === player_id
    // )[0];
    // if (!isEmpty(injury_info)) {
    //   injuryReport.push({
    //     Name: injury_info.Name,
    //     Position: injury_info.Position,
    //     BodyPart: injury_info.BodyPart,
    //     Status: injury_info.Status,
    //     Practice: injury_info.Practice,
    //     PracticeDescription: injury_info.PracticeDescription,
    //     Updated: injury_info.Updated,
    //     DeclaredInactive: injury_info.DeclaredInactive,
    //   });
    // }

    let stats_info = stats_data[player_id];
    let projections_info = projections_data[player_id];
    console.log(stats_info);
    console.log(projections_info);
    if (!isEmpty(stats_info)) {
      SeasonStats.push({
        playerName: stats_info.Name,
        position: stats_info.Position,
        team: stats_info.Team,
        passingYards: stats_info?.pass_yd,
        passingTDs: stats_info?.pass_td,
        // interceptions: stats_info.Interceptions,
        // receptions: stats_info.Receptions,
        receivingYards: stats_info.rec_yd,
        receivingTDs: stats_info.rec_td,
        SeasonProjectedPoints: projections_info?.pts_std,
      });
    }
  });

  standings.push({
    teamName: roster_info.teamName,
    wins: roster_info.wins,
    losses: roster_info.losses,
    pointsFor: roster_info.pointsFor,
    pointsAgainst: roster_info.pointsAgainst,
    rank: roster_info.rank,
    strengthOfSchedule: strengthOfSchedule,
    // injuryReport: injuryReport,
    playerStats: playerStats,
  });

  let output = {};
  output.leagueName = leagueData.name;
  output.numberOfTeams = rosterTeam.length;
  output.scoringSystem = leagueData.settings;

  output.standings = standings;

  // let prefix = `Fantasy Football is a simulation game where participants, known as team owners, create virtual teams composed of real-world NFL players. These teams earn points based on the real-world performance of their individual players.

  //   1. League Formation: Owners form a league, usually comprising 8-14 teams. Each team is managed by a different participant.

  //   2. Draft: During the draft, owners select real-world players to fill out their fantasy roster in positions like quarterback, running back, wide receiver, tight end, defense/special teams, and sometimes a kicker.

  //   3. Scoring: Each player on a fantasy team earns points individually based on their real-world performance, not by playing together as a team or against each other. Points are typically earned through touchdowns, yards gained, field goals, and more. All these individual points accumulate to form the fantasy team's overall score.

  //   4. Matchups and Season Structure: Each week, fantasy teams are paired off against each other, with the goal being to outscore the opposing fantasy team. The fantasy season aligns with the NFL regular season, and the teams with the best records at the end of the season advance to a playoff round, culminating in a championship matchup.

  //   The objective of fantasy football is to assemble and manage a team that will score the most points based on the real-world performances of the selected players. Success in the game requires a deep understanding of NFL player abilities, team dynamics, and game conditions.`

  let prefix = `Task: Hello, ChatGPT! Today you are an experienced fantasy football analyst and power rankings guru. With the help of the latest JSON object data, your job is to produce a compelling dialogue that brings the rich drama of the fantasy football league to life. Begin where your football knowledge guides you. Maybe it's the current league standings or perhaps a player who's shining in the player stats. Seamlessly move onto the details of team performances. Discuss the wins, losses, and the total points for and against. Dive into the strength of schedule that each team has faced. Consider then why certain teams are ranked as they are. Are they taking advantage of a favorable schedule or have they been struggling with injuries to key players? The injury report could be a great resource for this. Bring in the latest updates that could affect team performance for a fresh angle. If you're in a reflective mood, you could revisit past matches between the teams, reminiscing about their records and battles. It's also a perfect opportunity to share your strategies. You could suggest changes to the teams based on their rankings or point out unexpected players who might be about to shine. Keep the discussion focused on fantasy football: team selections, player health, and the impact of individual performance on team success. Remember to add a touch of humor and personal experiences to your conversation, maintaining an informal and engaging tone. Keep the language simple to encourage interaction. Please avoid decimals in your analysis - only whole numbers. Round up if the decimal is .5 or higher. Do not use phrases like 'average projection. Wrap up with your own predictions, the lessons from your experience, or a few bold statements. Use a variety of sentence structures and transitional phrases to ensure your narrative flows like a casual chat. Time to get in the game, ChatGPT! Let's make each power rankings analysis a unique and entertaining adventure.`;
  res.send({ prefix, output });
};

/**
 * Get opponents list
 * @param {Array} matchups
 * @param {Int} roster_id
 * @returns
 */
const get_opponents = (matchups, roster_id) => {
  let opponents_list = [];

  matchups.forEach((item) => {
    let team_matchups = item.matchups;

    // Get target team info by roster_id
    let target_team = team_matchups.filter(
      (matchup) => matchup.roster_id === roster_id
    )[0];

    // Get opponent team info by matchup_id
    let opponent_team = team_matchups.filter(
      (matchup) => matchup.matchup_id === target_team.matchup_id
    )[0];

    if (!opponents_list.includes(opponent_team.roster_id)) {
      opponents_list.push(opponent_team.roster_id);
    }
  });

  console.log(opponents_list);

  return opponents_list;
};

/**
 * Calculate strength of schedule
 * @param {Array} standings
 * @param {Int} team_name
 * @param {Array} opponents
 * @returns
 */
const calculate_strength_of_schedule = (standings, team_name, opponents) => {
  // Get the list of teams the input team has played against
  // let opponents = get_opponents(team_name);

  let total_wins = 0;
  let total_losses = 0;
  let total_points = 0;
  let total_games = 0;

  let min_points = 1000;
  let max_points = 0;
  // Sum up the total wins, total losses and total points of the opponents
  standings.forEach((team) => {
    if (opponents.includes(team.roster_id)) {
      total_wins += team.wins;
      total_losses += team.losses;
      total_points += team.pointsFor;
      total_games += team.wins + team.losses;

      if (min_points > team.pointsFor / (team.wins + team.losses)) {
        min_points = team.pointsFor / (team.wins + team.losses);
      }

      if (max_points < team.pointsFor / (team.wins + team.losses)) {
        max_points = team.pointsFor / (team.wins + team.losses);
      }
    }
  });

  // Calculate win-loss ratio
  let win_loss_ratio = total_wins / total_games;

  // Calculate average points scored by opponents
  let average_points_scored = total_points / total_games;

  // Assume we have a function to normalize these values so they're on the same scale
  let normalized_win_loss_ratio = normalize_win_loss_ratio(win_loss_ratio);
  let normalized_points_scored = normalize_points_scored(
    average_points_scored,
    min_points,
    max_points
  );

  // Calculate and return the strength of schedule as the average of the normalized values
  let strength_of_schedule =
    (normalized_win_loss_ratio + normalized_points_scored) / 2;

  return strength_of_schedule;
};

/**
 * Normalize win_loss_ratio
 * @param {float} win_loss_ratio
 * @returns
 */
const normalize_win_loss_ratio = (win_loss_ratio) => {
  // Since win_loss_ratio is already a value between 0 and 1, no normalization might be needed.
  // But just in case values are not strictly between 0 and 1, we can add a safety clamp.
  return clamp(win_loss_ratio, 0, 1);
};

/**
 * Normalize points_scored
 * @param {float} points
 * @param {float} min_points
 * @param {float} max_points
 * @returns
 */
const normalize_points_scored = (points, min_points, max_points) => {
  // Subtract the minimum number of points from the input points
  let normalized_points = points - min_points;
  // Divide by the range of possible points
  normalized_points = normalized_points / (max_points - min_points);
  return normalized_points;
};

module.exports = {
  generate_prompt,
};
