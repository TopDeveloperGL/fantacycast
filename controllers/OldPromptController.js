const JWT = require("jsonwebtoken");
const sleeper_package = require("sleeper_fantasy");
const isEmpty = require("is-empty");
const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_Matchups = require("../models/LeagueMatchups.model");
const League_User = require("../models/LeagueUser.model");
const WeekStats = require("../models/WeekStats.model");
const WeekProjections = require("../models/WeekProjections.model");
const SeasonStats = require("../models/SeasonStats.model");
const SeasonProjections = require("../models/SeasonProjections.model");
const ReportLog = require("../models/ReportLog.model");
const Report = require("../models/Report.model");
const WeekTransactions = require("../models/WeekTransactions.model");
const Player_News = require("../models/PlayerNews.model");
const completionContrller = require("./completionController");
const audioController = require("./audioController");
const {
  convert_owner_team_name,
  convert_player_name,
  convert_roster_team_name,
  get_standings,
  clamp,
  html_to_text,
  getNextMonday,
  calculateWeeks,
} = require("../utils/report");

const fetch = require("node-fetch");

/**
 * Get opponents list
 * @param {Array} matchups
 * @param {Int} roster_id
 * @returns
 */
const get_opponents = (matchups, roster_id) => {
  let opponents_list = [];

  matchups.forEach((item) => {
    let team_matchups = item.matchups;

    // Get target team info by roster_id
    let target_team = team_matchups.filter(
      (matchup) => matchup.roster_id === roster_id
    )[0];

    // Get opponent team info by matchup_id
    let opponent_team = team_matchups.filter(
      (matchup) => matchup.matchup_id === target_team.matchup_id
    )[0];

    if (!opponents_list.includes(opponent_team.roster_id)) {
      opponents_list.push(opponent_team.roster_id);
    }
  });

  console.log(opponents_list);

  return opponents_list;
};

/**
 * Calculate strength of schedule
 * @param {Array} standings
 * @param {Int} team_name
 * @param {Array} opponents
 * @returns
 */
const calculate_strength_of_schedule = (standings, team_name, opponents) => {
  // Get the list of teams the input team has played against
  // let opponents = get_opponents(team_name);

  let total_wins = 0;
  let total_losses = 0;
  let total_points = 0;
  let total_games = 0;

  let min_points = 1000;
  let max_points = 0;
  // Sum up the total wins, total losses and total points of the opponents
  standings.forEach((team) => {
    if (opponents.includes(team.roster_id)) {
      total_wins += team.wins;
      total_losses += team.losses;
      total_points += team.pointsFor;
      total_games += team.wins + team.losses;

      if (min_points > team.pointsFor / (team.wins + team.losses)) {
        min_points = team.pointsFor / (team.wins + team.losses);
      }

      if (max_points < team.pointsFor / (team.wins + team.losses)) {
        max_points = team.pointsFor / (team.wins + team.losses);
      }
    }
  });

  // Calculate win-loss ratio
  let win_loss_ratio = total_wins / total_games;

  // Calculate average points scored by opponents
  let average_points_scored = total_points / total_games;

  // Assume we have a function to normalize these values so they're on the same scale
  let normalized_win_loss_ratio = normalize_win_loss_ratio(win_loss_ratio);
  let normalized_points_scored = normalize_points_scored(
    average_points_scored,
    min_points,
    max_points
  );

  // Calculate and return the strength of schedule as the average of the normalized values
  let strength_of_schedule =
    (normalized_win_loss_ratio + normalized_points_scored) / 2;

  return strength_of_schedule;
};

/**
 * Normalize win_loss_ratio
 * @param {float} win_loss_ratio
 * @returns
 */
const normalize_win_loss_ratio = (win_loss_ratio) => {
  // Since win_loss_ratio is already a value between 0 and 1, no normalization might be needed.
  // But just in case values are not strictly between 0 and 1, we can add a safety clamp.
  return clamp(win_loss_ratio, 0, 1);
};

/**
 * Normalize points_scored
 * @param {float} points
 * @param {float} min_points
 * @param {float} max_points
 * @returns
 */
const normalize_points_scored = (points, min_points, max_points) => {
  // Subtract the minimum number of points from the input points
  let normalized_points = points - min_points;
  // Divide by the range of possible points
  normalized_points = normalized_points / (max_points - min_points);
  return normalized_points;
};

const trade_analysis = (transaction, player_name, player_data, roster_name) => {
  let { type, status, roster_ids, drops, adds, created } = transaction;

  let trans = {};
  let adds_data = Object.entries(adds);
  let drops_data = Object.entries(drops);

  let change_adds = adds_data.map((add) => {
    return [player_name[add[0]], roster_name[add[1]]];
  });

  let change_drops = drops_data.map((add) => {
    return [player_name[add[0]], roster_name[add[1]]];
  });

  trans.tradeDate = created;
  trans.drops = Object.fromEntries(change_drops);
  trans.adds = Object.fromEntries(change_adds);
  trans.playersInvolved = [];

  drops_data.map((drop) => {
    // trans.tradedFrom = drop[1];
    // trans.tradedTo = adds_data.filter(add => add[0] === drop[0])[0][1];
    trans.playersInvolved.push({
      playerName: player_name[drop[0]],
      position: player_data[drop[0]].position,
    });
  });

  return trans;
};

const generatePreMatchupAnalysis = async (req) => {
  let { league, week, matchup, reportId, recipient, voiceId, decoded } = req;
  const sleeper_instance = new sleeper_package.sleeper();

  let playerData = [];
  if (await Player.count()) {
    playerData = await Player.find();
  } else {
    const fetch_player = await sleeper_instance.players.fetch_all_players();
    let player_value = fetch_player.all_players;
    playerData = Object.keys(player_value).map((key) => {
      return { ...player_value[key] };
    });
    Player.insertMany(playerData);
  }


  const league_ids = [league];
  sleeper_instance.leagues = league_ids;

  let leagueData = {};
  if (await League.count()) {
    leagueData = await League.findOne({ league_id: league });
  } else {
    const fetch_leagues = await Promise.all(sleeper_instance.league_promises);

    leagueData = fetch_leagues[0];
    League.create(leagueData);
  }


  if (decoded.user_role === 1) {
    let nextMonday = getNextMonday(`${process.env.START_DATE}`);
    let todayDay = new Date().toISOString().split("T")[0];
    if (new Date(nextMonday).getTime() > new Date(todayDay).getTime()) {
      week = 1;
    } else {
      week = calculateWeeks(
        getNextMonday(`${process.env.START_DATE}`),
        new Date().toISOString().split("T")[0]
      );
    }
  }

  let rosterData = [];
  if (await League_Roster.count()) {
    rosterData = await League_Roster.find({ league_id: league });
  } else {
    const fetch_rosters = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_rosters();

    rosterData = fetch_rosters.rosters;
    League_Roster.insertMany(rosterData);
  }


  let userData = [];
  if (await League_User.count()) {
    userData = await League_User.find({ league_id: league });
  } else {
    const fetch_users = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_owners();

    userData = fetch_users.owners;
    League_User.insertMany(userData);
  }

  let matchupData = [];
  if (await League_Matchups.count()) {
    let matchups_by_week = await League_Matchups.findOne({
      league_id: league,
      week: week,
    });
    matchupData = matchups_by_week?.matchups;
  }
  if (isEmpty(matchupData)) {
    let fetch_matchups = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_matchups(week);

    matchupData = fetch_matchups.matchups;
    League_Matchups.create({
      league_id: leagueData.league_id,
      week: week,
      matchups: matchupData,
    });
  }

  if (decoded.user_role === 1) {
    userData.map((user) => {
      if (user.display_name === decoded.sleeper_id) {
        let teamRoster = rosterData?.filter(
          (roster) => roster.owner_id === user.user_id
        )[0];
        let teamMatchup = matchupData?.filter(
          (matchup) => matchup.roster_id === teamRoster.roster_id
        )[0];

        matchup = teamMatchup.matchup_id;
      }
    });
  }

  let stats_data = {};
  if (week > 1) {
    let stats_by_week = {};

    if (await WeekStats.count()) {
      stats_by_week = await WeekStats.findOne({
        season: leagueData.season,
        season_type: leagueData.season_type,
        week: week - 1,
      });
      stats_data = stats_by_week?.stats;
    }

    if (isEmpty(stats_by_week)) {
      let fetch_stats = await sleeper_instance.players.fetch_stats(
        leagueData.season_type,
        leagueData.season,
        week - 1
      );

      WeekStats.create({
        season: leagueData.season,
        season_type: leagueData.season_type,
        week: week - 1,
        stats: fetch_stats.stats,
      });

      stats_data = fetch_stats.stats;
    }
  }

  let projections_data = {};
  let projections_by_week = {};

  if (await WeekProjections.count()) {
    projections_by_week = await WeekProjections.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
      week: week,
    });

    projections_data = projections_by_week?.projections;
  }

  if (isEmpty(projections_by_week)) {
    let fetch_projections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season,
      week
    );

    WeekProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      week: week,
      projections: fetch_projections.projections,
    });

    projections_data = fetch_projections.projections;
  }

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_team_name = convert_roster_team_name(rosterData, owner_team_name);

  let matchup_team = matchupData?.filter(
    // (item) => `${item.matchup_id}` === matchup
    (item) => item.matchup_id == matchup
  );

  let teams = [];
  matchup_team.forEach((item) => {
    let team_info = {};
    team_info.team_name = roster_team_name[item.roster_id];
    team_info.roster = {};
    let players_info = [];

    let player_ids = item.players;

    player_ids.forEach((player_id) => {
      let stats_info = stats_data[player_id];
      let projections_info = projections_data[player_id];
      let player_info = playerData.filter(
        (item) => item.player_id === player_id
      )[0];

      let projectionsWeekAverage = {
        rushing_yards: projections_info?.rush_yd,
        rushing_touchdowns: projections_info?.rush_td,
        receiving_yards: projections_info?.rec_yd,
        receiving_touchdowns: projections_info?.rec_td,
        passing_yards: projections_info?.pass_yd,
        passing_touchdowns: projections_info?.pass_td,
        fantasy_points: projections_info?.pts_std,
      };

      let previousWeekStats = {
        rushing_yards: stats_info?.rush_yd,
        rushing_touchdowns: stats_info?.rush_td,
        receiving_yards: stats_info?.rec_yd,
        receiving_touchdowns: stats_info?.rec_td,
        passing_yards: stats_info?.pass_yd,
        passing_touchdowns: stats_info?.pass_td,
        fantasy_points: stats_info?.pts_std,
      };
      players_info.push({
        name: player_info?.full_name,
        position: player_info?.position,
        status: player_info?.status,
        depthPosition: player_info?.depth_chart_position,
        projectionsWeekAverage: projectionsWeekAverage,
        previousWeekStats: previousWeekStats,
      });
    });

    let roster_team = rosterData.filter(
      (roster) => roster.roster_id === item.roster_id
    )[0];
    team_info.roster.players = players_info;
    team_info.record = {};

    let record = {
      wins: roster_team.settings.wins,
      losses: roster_team.settings.losses,
      streak: roster_team.metadata.streak,
      fpts: roster_team.settings.fpts,
      fpts_against: roster_team.settings.fpts_against,
    };
    team_info.starters = item.starters?.map((pl) => player_name[pl]);
    team_info.reserve = item.reserve?.map((pl) => player_name[pl]);
    team_info.record = record;
    teams.push(team_info);
  });

  let output = {};
  output.matchupId = matchup;
  output.matchupWeek = week;
  output.teams = rotoballer_player_news;

  let prefix = `
  Prompt:
  Hey there, ChatGPT! Let's dive into the exciting world of Fantasy Football. Your role here is like a skillful storyteller, who needs to use the NFL data for the week, available in JSON format, and weave it into a conversation-like review. The flow should be natural, random, and authentic—just like a spontaneous chat with a fellow football enthusiast. Start the chat with whatever catches your fancy. Maybe it's the latest win-loss records or perhaps a star player who's on fire or one that's been fumbling. From there, smoothly move onto the nuances of player performances. Discuss their positions, weekly projections, or rankings based on intriguing stats like rushing yards, receiving yards, or consistent scores. But remember to avoid using the term 'average projection'. Perhaps then you'd like to dissect why certain star players are projected to perform the way they are. Are they on a hot streak or exploiting their opponents' weaknesses? Or you might decide to talk about the role of external factors like stadium conditions. And what about the latest Rotoballer news? Important updates that could shake up the player performances can be an interesting topic. If you're feeling nostalgic, delve into past epic encounters between the teams, remembering their results and rivalries. You could then switch gears and share your own strategies—suggestions for lineup changes or perhaps some benchwarmers who are diamonds in the rough. Keep the discussion rooted in fantasy football gameplay, addressing areas like lineup optimization, player health, performance-based points, possible trades, and the waiver wire. Remember to weave humor and personal stories into your narrative, keeping the tone light and conversational. Use simple language that invites interaction. Please refrain from including decimals in your review. Stick to whole numbers only. If you have a decimal of .5 or higher, make sure to round it up. Avoid using phrases like 'average projection.' Lastly, tie things up with your key insights, your wisdom, and even your boldest predictions. Use a variety of sentence structures and transition words to ensure your report flows like a friendly conversation. Ready to kick off, ChatGPT? Let's make every report a fresh and unique experience!
  DATA:${output}
`;
  let description = `${prefix}\n\n${JSON.stringify(output)}`;

  let gptContext = await completionContrller.generateCompletion(description);
  // let gptContext = "Hello"
  // let date = new Date();
  // let startDate = date.toISOString().split("T")[0];
  // let endDate = date.setDate(date.getDate() + 1);
  // endDate = new Date(endDate).toISOString().split("T")[0];

  // console.log(new Date(startDate), new Date(endDate));

  // let isReport = await ReportLog.aggregate([
  //   {
  //     $match: {
  //       run_date: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(isReport);
  let data = {
    league_id: league,
    report_id: reportId,
    user_id: decoded.id,
    content: gptContext,
    is_run: true,
    is_schedule: false,
    run_date: new Date(),
    schedule_date: null,
    recipient: recipient,
  };
  let savedReportLog = await ReportLog.create(data);
  let reportData = await Report.findById(reportId);
  console.log(gptContext);

  return {
    id: savedReportLog._id,
    name: reportData.name,
    content: gptContext,
  };
};

const generatePostMatchupAnalysis = async (req) => {
  let { league, week, matchup, reportId, recipient, voiceId, decoded } = req;

  const sleeper_instance = new sleeper_package.sleeper();

  let playerData = [];
  if (await Player.count()) {
    playerData = await Player.find();
  } else {
    const fetch_player = await sleeper_instance.players.fetch_all_players();
    let player_value = fetch_player.all_players;
    playerData = Object.keys(player_value).map((key) => {
      return { ...player_value[key] };
    });
    Player.insertMany(playerData);
  }

  const league_ids = [league];
  sleeper_instance.leagues = league_ids;

  let leagueData = {};
  if (await League.count()) {
    leagueData = await League.findOne({ league_id: league });
  } else {
    const fetch_leagues = await Promise.all(sleeper_instance.league_promises);

    leagueData = fetch_leagues[0];
    League.create(leagueData);
  }

  if (decoded.user_role === 1) {
    let nextMonday = getNextMonday(`${process.env.START_DATE}`);
    let todayDay = new Date().toISOString().split("T")[0];
    console.log(new Date(nextMonday).getTime(), new Date(todayDay).getTime());
    if (new Date(nextMonday).getTime() > new Date(todayDay).getTime()) {
      week = 1;
    } else {
      week = calculateWeeks(
        getNextMonday(`${process.env.START_DATE}`),
        new Date().toISOString().split("T")[0]
      );
    }
  }

  let rosterData = [];
  if (await League_Roster.count()) {
    rosterData = await League_Roster.find({ league_id: league });
  } else {
    const fetch_rosters = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_rosters();

    rosterData = fetch_rosters.rosters;
    League_Roster.insertMany(rosterData);
  }

  let userData = [];
  if (await League_User.count()) {
    userData = await League_User.find({ league_id: league });
  } else {
    const fetch_users = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_owners();

    userData = fetch_users.owners;
    League_User.insertMany(userData);
  }

  let matchupData = [];
  if (await League_Matchups.count()) {
    let matchups_by_week = await League_Matchups.findOne({
      league_id: league,
      week: week,
    });
    matchupData = matchups_by_week?.matchups;
  }
  if (isEmpty(matchupData)) {
    let fetch_matchups = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_matchups(week);

    matchupData = fetch_matchups.matchups;
    League_Matchups.create({
      league_id: leagueData.league_id,
      week: week,
      matchups: matchupData,
    });
  }
  if (decoded.user_role === 1) {
    userData.map((user) => {
      if (user.display_name === decoded.sleeper_id) {
        let teamRoster = rosterData?.filter(
          (roster) => roster.owner_id === user.user_id
        )[0];
        let teamMatchup = matchupData?.filter(
          (matchup) => matchup.roster_id === teamRoster.roster_id
        )[0];
        matchup = teamMatchup.matchup_id;
      }
    });
    console.log(week, matchup);
  }

  let stats_data = {};
  if (week > 1) {
    let stats_by_week = {};

    if (await WeekStats.count()) {
      stats_by_week = await WeekStats.findOne({
        season: leagueData.season,
        season_type: leagueData.season_type,
        week: week - 1,
      });
      stats_data = stats_by_week?.stats;
    }

    if (isEmpty(stats_by_week)) {
      let fetch_stats = await sleeper_instance.players.fetch_stats(
        leagueData.season_type,
        leagueData.season,
        week - 1
      );

      WeekStats.create({
        season: leagueData.season,
        season_type: leagueData.season_type,
        week: week - 1,
        stats: fetch_stats.stats,
      });

      stats_data = fetch_stats.stats;
    }
  }

  let nowStatsData = {};
  let nowStatsByWeek = {};
  // let nowStatsCount = await WeekStats.count();
  // if (await WeekStats.count()) {
  //   nowStatsByWeek = await WeekStats.findOne({
  //     season: leagueData.season,
  //     season_type: leagueData.season_type,
  //     week: week,
  //     stats: leagueData.stats
  //   });
  //   nowStatsData = nowStatsByWeek?.stats;
  // }

  if (isEmpty(nowStatsByWeek)) {
    let fetch_stats = await sleeper_instance.players.fetch_stats(
      leagueData.season_type,
      leagueData.season,
      week
    );
    // console.log(fetch_stats.stats);
    WeekStats.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      week: week,
      stats: fetch_stats.stats,
    });

    nowStatsData = fetch_stats.stats;
  }

  let projections_data = {};
  let projections_by_week = {};

  if (await WeekProjections.count()) {
    projections_by_week = await WeekProjections.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
      week: week,
    });

    projections_data = projections_by_week?.projections;
  }

  if (isEmpty(projections_by_week)) {
    let fetch_projections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season,
      week
    );

    WeekProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      week: week,
      projections: fetch_projections.projections,
    });

    projections_data = fetch_projections.projections;
  }

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_team_name = convert_roster_team_name(rosterData, owner_team_name);

  let standings_data = get_standings(rosterData, roster_team_name);

  let matchup_team = matchupData?.filter((item) => item.matchup_id === matchup);

  let recentMatches = {};
  let scoreMax = 0;
  let teams = [];
  matchup_team.forEach((item, idx) => {
    recentMatches[`teamName${idx + 1}`] = roster_team_name[item.roster_id];
    recentMatches[`team${idx + 1}Score`] = item.points;
    if (scoreMax < item.points) {
      scoreMax = item.points;
      recentMatches.winner = roster_team_name[item.roster_id];
    }

    let team_info = {};

    let players_info = [];

    let player_ids = item.players;

    let pointsData = item.players_points;

    player_ids.forEach((player_id) => {
      let stats_info = stats_data[player_id];
      let projections_info = projections_data[player_id];
      let player_info = playerData.filter(
        (item) => item.player_id === player_id
      )[0];
      let projectionsWeekAverage = {
        rushing_yards: projections_info?.rush_yd,
        rushing_touchdowns: projections_info?.rush_td,
        receiving_yards: projections_info?.rec_yd,
        receiving_touchdowns: projections_info?.rec_td,
        passing_yards: projections_info?.pass_yd,
        passing_touchdowns: projections_info?.pass_td,
        fantasy_points: projections_info?.pts_std,
      };

      let previousWeekStats = {
        rushing_yards: stats_info?.rush_yd,
        rushing_touchdowns: stats_info?.rush_td,
        receiving_yards: stats_info?.rec_yd,
        receiving_touchdowns: stats_info?.rec_td,
        passing_yards: stats_info?.pass_yd,
        passing_touchdowns: stats_info?.pass_td,
        fantasy_points: stats_info?.pts_std,
      };
      players_info.push({
        name: player_info?.full_name,
        position: player_info?.position,
        pointsScoredThisMatch: pointsData?.player_id,
        projectionsWeekAverage: projectionsWeekAverage,
        previousWeekStats: previousWeekStats,
      });
    });

    let roster_team = rosterData.filter(
      (roster) => roster.roster_id === item.roster_id
    )[0];

    team_info.teamName = roster_team_name[item.roster_id];
    team_info.rank = standings_data.filter(
      (sub_item) => sub_item.roster_id === item.roster_id
    )[0].rank;
    team_info.wins = roster_team.settings.wins;
    team_info.losses = roster_team.settings.losses;
    team_info.pointsFor = roster_team.settings.fpts;
    team_info.pointsAgainst = roster_team.settings.fpts_against;

    team_info.playerStats = players_info;
    teams.push(team_info);
  });

  let output = {};
  output.leagueName = leagueData.name;
  output.numberOfTeams = 2;
  output.scoringSystem = leagueData.settings;
  output.recentMatches = recentMatches;
  output.standings = teams;

  let prefix = `Hey there, ChatGPT! Let's huddle up and break down the latest matchups from the world of Fantasy Football. Your role is like that of a passionate commentator, using the NFL's latest stats and outcomes, available in JSON format, to craft a spontaneous post-game chat. The discussion should be unpredictable, genuine, and fluid—like a catch-up with an old friend after watching a game.Why not kick off with a few surprises from the recent games? Maybe a team that defied expectations or an underdog player who truly shone. From there, it's a deep dive into the player stats. Explore their positions, actual points scored, or rankings based on captivating metrics like rushing or receiving yards. Instead of "average projection," focus on terms like "actual performance."Was there a player who defied all predictions or another who unexpectedly underperformed? Ponder upon their recent form and matchups—were they up against a traditionally strong defense or did external factors, say weather conditions, play a role? Also, don't forget to check in on the latest Rotoballer updates. Any major events that influenced the game outcomes?Feeling reminiscent? Take a moment to reflect on historical matchups between the teams, evoking memories of their old clashes and feuds. Then, maybe you'd like to dissect the fantasy implications of the results. Share insights on lineup adjustments post-match, or shine a light on players who, despite a quiet game, still hold potential for future matchups.Keep the chat lively with anecdotes, humor, and perhaps a few wild speculations about the next round. Use simple and engaging language, and remember to stick to whole numbers. If you encounter decimals of .5 or higher, round them up.Wrap it all up with your key observations, lessons learned from the week, and maybe a few audacious forecasts for the upcoming games. Make sure to incorporate diverse sentence structures and transition words to ensure your analysis flows seamlessly.Are you all set, ChatGPT? Let’s break down the game week, play by play!
  
  DATA:
  ${output}
  `;
  let description = `${prefix}\n\n${JSON.stringify(output)}`;

  let gptContext = await completionContrller.generateCompletion(description);
  // let date = new Date();
  // let startDate = date.toISOString().split("T")[0];
  // let endDate = date.setDate(date.getDate() + 1);
  // endDate = new Date(endDate).toISOString().split("T")[0];

  // console.log(new Date(startDate), new Date(endDate));

  // let isReport = await ReportLog.aggregate([
  //   {
  //     $match: {
  //       run_date: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(isReport);
  let data = {
    league_id: league,
    report_id: reportId,
    user_id: decoded.id,
    content: gptContext,
    is_run: true,
    is_schedule: false,
    run_date: new Date(),
    schedule_date: null,
    recipient: recipient,
  };
  let savedReportLog = await ReportLog.create(data);
  let reportData = await Report.findById(reportId);
  await audioController.generateFile(voiceId, gptContext, savedReportLog._id);

  return {
    id: savedReportLog._id,
    name: reportData.name,
    content: gptContext,
  };
};

const generatePowerRanking = async (req) => {
  let { league, team, reportId, recipient, voiceId, decoded } = req;

  const sleeper_instance = new sleeper_package.sleeper();

  let playerData = [];
  if (await Player.count()) {
    playerData = await Player.find();
  } else {
    const fetch_player = await sleeper_instance.players.fetch_all_players();
    let player_value = fetch_player.all_players;
    playerData = Object.keys(player_value).map((key) => {
      return { ...player_value[key] };
    });
    Player.insertMany(playerData);
  }

  const league_ids = [league];
  sleeper_instance.leagues = league_ids;

  let leagueData = {};
  if (await League.count()) {
    leagueData = await League.findOne({ league_id: league });
  } else {
    const fetch_leagues = await Promise.all(sleeper_instance.league_promises);

    leagueData = fetch_leagues[0];
    League.create(leagueData);
  }

  // if (decoded.user_role === 1) {
  //   let nextMonday = getNextMonday(`${process.env.START_DATE}`);
  //   let todayDay = new Date().toISOString().split("T")[0];
  //   console.log(new Date(nextMonday).getTime(), new Date(todayDay).getTime());
  //   if (new Date(nextMonday).getTime() > new Date(todayDay).getTime()) {
  //     week = 1;
  //   } else {
  //     week = calculateWeeks(
  //       getNextMonday(`${process.env.START_DATE}`),
  //       new Date().toISOString().split("T")[0]
  //     );
  //   }
  // }

  let rosterData = [];
  if (await League_Roster.count()) {
    rosterData = await League_Roster.find({ league_id: league });
  } else {
    const fetch_rosters = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_rosters();

    rosterData = fetch_rosters.rosters;
    League_Roster.insertMany(rosterData);
  }

  let userData = [];
  if (await League_User.count()) {
    userData = await League_User.find({ league_id: league });
  } else {
    const fetch_users = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_owners();

    userData = fetch_users.owners;
    League_User.insertMany(userData);
  }

  if (decoded.user_role === 1) {
    let teamUser = userData.filter(
      (user) => user.display_name === decoded.sleeper_id
    )[0];
    team = teamUser.user_id;
  }

  let rosterTeam = await League_Roster.findOne({
    league_id: league,
    owner_id: team,
  });

  let matchupData = [];
  if (await League_Matchups.count()) {
    matchupData = await League_Matchups.find({
      league_id: league,
    });
  }

  // let weeks = [];
  // for (let i = 1; i <= 18; i++) {
  //   weeks.push(i);
  // }

  // let injuries_weeks = [];
  // await Promise.all(
  //   weeks?.map(async (item) => {
  //     let injuries_by_week = await Injuries.findOne({
  //       season: leagueData.season,
  //       season_type: 1,
  //       week: item,
  //     });

  //     let injuries_team = injuries_by_week?.data;

  //     if (isEmpty(injuries_by_week)) {
  //       let url = `https://api.sportsdata.io/v3/nfl/stats/json/Injuries/${leagueData.season}/${item}?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //       await axios(url)
  //         .then((injuries) => {
  //           Injuries.create({
  //             season: leagueData.season,
  //             season_type: 1,
  //             week: item,
  //             data: injuries.data,
  //           });
  //           injuries_team = injuries.data;
  //         })
  //         .catch((err) => console.log(err));
  //     }
  //     injuries_weeks = injuries_weeks.concat(injuries_team);
  //   })
  // );

  // console.log(injuries_weeks.length);

  // let teams_list = await Teams.find();

  let stats_data = {};
  let stats_by_season = {};
  if (await SeasonStats.count()) {
    stats_by_season = await SeasonStats.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
    });

    stats_data = stats_by_season?.stats;
  }

  if (isEmpty(stats_by_season)) {
    let fetchStats = await sleeper_instance.players.fetch_stats(
      leagueData.season_type,
      leagueData.season
    );
    SeasonStats.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      stats: fetchStats.stats,
    });
    stats_data = fetchStats.stats;
  }

  let projections_data = {};
  let projections_by_season = {};

  if (await SeasonProjections.count()) {
    projections_by_season = await SeasonProjections.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
    });

    projections_data = projections_by_season?.projections;
  }

  if (isEmpty(projections_by_season)) {
    let fetchProjections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season
    );
    SeasonProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      projections: fetchProjections.projections,
    });

    projections_data = fetchProjections.projections;
  }

  let players = [];
  playerData.forEach((item) => {
    players.push([item.player_id, item]);
  });

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_name = convert_roster_team_name(rosterData, owner_team_name);

  let standings_data = get_standings(rosterData, roster_name);

  let strengthOfSchedule = calculate_strength_of_schedule(
    standings_data,
    rosterTeam.roster_id,
    get_opponents(matchupData, rosterTeam.roster_id)
  );

  let standings = [];
  let roster_info = standings_data.filter(
    (sub_item) => sub_item.roster_id === rosterTeam.roster_id
  )[0];

  // let player_ids = rosterData.filter(
  //   (team) => team.roster_id === rosterTeam.roster_id
  // )[0].players;
  let player_ids = rosterTeam.players;

  // let injuryReport = [];
  let playerStats = [];
  player_ids.forEach((player_id) => {
    // let injury_info = injuries_weeks.filter(
    //   (injury) => injury.PlayerID === player_id
    // )[0];
    // if (!isEmpty(injury_info)) {
    //   injuryReport.push({
    //     Name: injury_info.Name,
    //     Position: injury_info.Position,
    //     BodyPart: injury_info.BodyPart,
    //     Status: injury_info.Status,
    //     Practice: injury_info.Practice,
    //     PracticeDescription: injury_info.PracticeDescription,
    //     Updated: injury_info.Updated,
    //     DeclaredInactive: injury_info.DeclaredInactive,
    //   });
    // }

    let stats_info = stats_data[player_id];
    let projections_info = projections_data[player_id];
 
    if (!isEmpty(stats_info)) {
      playerStats.push({
        playerName: stats_info.Name,
        position: stats_info.Position,
        team: stats_info.Team,
        passingYards: stats_info?.pass_yd,
        passingTDs: stats_info?.pass_td,
        receivingYards: stats_info.rec_yd,
        receivingTDs: stats_info.rec_td,
        SeasonProjectedPoints: projections_info?.pts_std,
      });
    }
  });

  standings.push({
    teamName: roster_info.teamName,
    wins: roster_info.wins,
    losses: roster_info.losses,
    pointsFor: roster_info.pointsFor,
    pointsAgainst: roster_info.pointsAgainst,
    rank: roster_info.rank,
    strengthOfSchedule: strengthOfSchedule,
    // injuryReport: injuryReport,
    playerStats: playerStats,
  });

  let output = {};
  output.leagueName = leagueData.name;
  output.numberOfTeams = rosterData.length;
  output.scoringSystem = leagueData.settings;

  output.standings = standings;

  let prefix = `Task: Hello, ChatGPT! Today you are an experienced fantasy football analyst and power rankings guru. With the help of the latest JSON object data, your job is to produce a compelling dialogue that brings the rich drama of the fantasy football league to life. Begin where your football knowledge guides you. Maybe it's the current league standings or perhaps a player who's shining in the player stats. Seamlessly move onto the details of team performances. Discuss the wins, losses, and the total points for and against. Dive into the strength of schedule that each team has faced. Consider then why certain teams are ranked as they are. Are they taking advantage of a favorable schedule or have they been struggling with injuries to key players? The injury report could be a great resource for this. Bring in the latest updates that could affect team performance for a fresh angle. If you're in a reflective mood, you could revisit past matches between the teams, reminiscing about their records and battles. It's also a perfect opportunity to share your strategies. You could suggest changes to the teams based on their rankings or point out unexpected players who might be about to shine. Keep the discussion focused on fantasy football: team selections, player health, and the impact of individual performance on team success. Remember to add a touch of humor and personal experiences to your conversation, maintaining an informal and engaging tone. Keep the language simple to encourage interaction. Please avoid decimals in your analysis - only whole numbers. Round up if the decimal is .5 or higher. Do not use phrases like 'average projection. Wrap up with your own predictions, the lessons from your experience, or a few bold statements. Use a variety of sentence structures and transitional phrases to ensure your narrative flows like a casual chat. Time to get in the game, ChatGPT! Let's make each power rankings analysis a unique and entertaining adventure.
  
  DATA:
  ${output}
  `;
  let description = `${prefix}\n\n${JSON.stringify(output)}`;
  let gptContext = await completionContrller.generateCompletion(description);
  // let date = new Date();
  // let startDate = date.toISOString().split("T")[0];
  // let endDate = date.setDate(date.getDate() + 1);
  // endDate = new Date(endDate).toISOString().split("T")[0];

  // console.log(new Date(startDate), new Date(endDate));

  // let isReport = await ReportLog.aggregate([
  //   {
  //     $match: {
  //       run_date: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(isReport);
  let data = {
    league_id: league,
    report_id: reportId,
    user_id: decoded.id,
    content: gptContext,
    is_run: true,
    is_schedule: false,
    run_date: new Date(),
    schedule_date: null,
    recipient: recipient,
  };
  let savedReportLog = await ReportLog.create(data);
  let reportData = await Report.findById(reportId);
  await audioController.generateFile(voiceId, gptContext, savedReportLog._id);

  return {
    id: savedReportLog._id,
    name: reportData.name,
    content: gptContext,
  };
};

const generateTeamAnalysis = async (req) => {};

const generateTransactionAnalysis = async (req) => {
  let { league, weeks, reportId, recipient, voiceId, decoded } = req;

  const sleeper_instance = new sleeper_package.sleeper();

  let playerData = [];
  if (await Player.count()) {
    playerData = await Player.find();
  } else {
    const fetch_player = await sleeper_instance.players.fetch_all_players();
    let player_value = fetch_player.all_players;
    playerData = Object.keys(player_value).map((key) => {
      return { ...player_value[key] };
    });
    Player.insertMany(playerData);
  }

  const league_ids = [league];
  sleeper_instance.leagues = league_ids;

  let leagueData = {};
  if (await League.count()) {
    leagueData = await League.findOne({ league_id: league });
  } else {
    const fetch_leagues = await Promise.all(sleeper_instance.league_promises);

    leagueData = fetch_leagues[0];
    League.create(leagueData);
  }

  let matchupData = [];
  if (await League_Matchups.count()) {
    matchupData = await League_Matchups.find({
      league_id: league,
    });
  }

  let rosterData = [];
  if (await League_Roster.count()) {
    rosterData = await League_Roster.find({ league_id: league });
  } else {
    const fetch_rosters = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_rosters();

    rosterData = fetch_rosters.rosters;
    League_Roster.insertMany(rosterData);
  }

  let userData = [];
  if (await League_User.count()) {
    userData = await League_User.find({ league_id: league });
  } else {
    const fetch_users = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_owners();

    userData = fetch_users.owners;
    League_User.insertMany(userData);
  }

  let week_info = [];
  let weekData = weeks;
  console.log(weekData);
  await Promise.all(
    weekData.map(async (week) => {
      let transactionData = [];
      let transaction_count = await WeekTransactions.count({
        league_id: league,
        week: week,
      });
      if (!transaction_count) {
        let fetchTransaction = await sleeper_instance.leagues[
          league
        ].fetch_transactions(week);
        transactionData = fetchTransaction.transactions;
        await WeekTransactions.create({
          league_id: league,
          week: week,
          transactions: transactionData,
        });
      }
    })
  );
  weekData.forEach((item) => {
    week_info.push({ week: item });
  });
  console.log(week_info);

  let transaction_database = await WeekTransactions.aggregate([
    {
      $match: { league_id: league },
    },
    {
      $match: {
        $or: week_info,
      },
    },
  ]);

  // let injuries_weeks = [];
  // await Promise.all(
  //   req.body.weeks?.map(async (item) => {
  //     let injuries_by_week = await Injuries.findOne({
  //       season: leagueData.season,
  //       season_type: 1,
  //       week: item[0],
  //     });

  //     let injuries_team = injuries_by_week?.data;

  //     if (isEmpty(injuries_by_week)) {
  //       let url = `https://api.sportsdata.io/v3/nfl/stats/json/Injuries/${leagueData.season}/${item[0]}?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //       await axios(url)
  //         .then((injuries) => {
  //           Injuries.create({
  //             season: leagueData.season,
  //             season_type: 1,
  //             week: item[0],
  //             data: injuries.data,
  //           });
  //           injuries_team = injuries.data;
  //         })
  //         .catch((err) => console.log(err));
  //     }
  //     injuries_weeks = injuries_weeks.concat(injuries_team);
  //   })
  // );
  // console.log(injuries_weeks.length);

  // let injuries_by_weeks = await Injuries.aggregate([
  //   {
  //     $match: {
  //       season: leagueData.season,
  //       season_type: 1,
  //     },
  //   },
  //   {
  //     $match: {
  //       $or: week_info,
  //     },
  //   },
  // ]);

  let stats_data = {};
  let stats_by_season = {};
  if (await SeasonStats.count()) {
    stats_by_season = await SeasonStats.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
    });

    stats_data = stats_by_season?.stats;
  }

  if (isEmpty(stats_by_season)) {
    let fetchStats = await sleeper_instance.players.fetch_stats(
      leagueData.season_type,
      leagueData.season
    );
    SeasonStats.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      stats: fetchStats.stats,
    });
    stats_data = fetchStats.stats;
  }
  // console.log(stats_data);

  let projections_data = {};
  let projections_by_season = {};

  if (await SeasonProjections.count()) {
    projections_by_season = await SeasonProjections.findOne({
      season: leagueData.season,
      season_type: leagueData.season_type,
    });

    projections_data = projections_by_season?.projections;
  }

  if (isEmpty(projections_by_season)) {
    let fetchProjections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season
    );
    SeasonProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      projections: fetchProjections.projections,
    });

    projections_data = fetchProjections.projections;
  }

  let players = [];
  playerData.forEach((item) => {
    players.push([item.player_id, item]);
  });

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_name = convert_roster_team_name(rosterData, owner_team_name);

  let tradeTransactions = [];

  let roster_list = [];
  console.log(transaction_database);
  transaction_database.forEach((item) => {
    item.transactions?.forEach((transaction) => {
      if (transaction.status === "complete" && transaction.type === "trade") {
        transaction.roster_ids?.forEach((item) => {
          if (!roster_list.includes(item)) {
            roster_list.push(item);
          }
        });
        // console.log(transaction);
        let tran_info = trade_analysis(
          transaction,
          player_name,
          Object.fromEntries(players),
          roster_name
        );
        tradeTransactions.push(tran_info);
      }
    });
  });

  let standings_data = get_standings(rosterData, roster_name);

  let standings = [];
  roster_list.forEach((roster_id) => {
    let strengthOfSchedule = calculate_strength_of_schedule(
      standings_data,
      roster_id,
      get_opponents(matchupData, roster_id)
    );

    let roster_info = standings_data.filter(
      (sub_item) => sub_item.roster_id === roster_id
    )[0];

    let players = rosterData.filter((team) => team.roster_id === roster_id)[0]
      .players;

    // let injuryReport = [];
    let playerStats = [];
    players.forEach((player_id) => {
      // let injury_info = injuries_weeks.filter(
      //   (injury) => injury.PlayerID === player_id
      // )[0];
      // if (!isEmpty(injury_info)) {
      //   injuryReport.push({
      //     Name: injury_info.Name,
      //     Position: injury_info.Position,
      //     BodyPart: injury_info.BodyPart,
      //     Status: injury_info.Status,
      //     Practice: injury_info.Practice,
      //     PracticeDescription: injury_info.PracticeDescription,
      //     Updated: injury_info.Updated,
      //     DeclaredInactive: injury_info.DeclaredInactive,
      //   });
      // }
      let player_info = playerData.filter(
        (item) => item.player_id === player_id
      )[0];
      let stats_info = stats_data[player_id];
      let projections_info = projections_data[player_id];
      if (!isEmpty(stats_info)) {
        playerStats.push({
          playerName: player_info?.full_name,
          position: player_info?.position,
          passingYards: stats_info?.pass_yd,
          passingTDs: stats_info?.pass_td,
          receivingYards: stats_info?.rec_yd,
          receivingTDs: stats_info?.rec_td,
          SeasonProjectedPoints: projections_info?.pts_std,
        });
      }
    });

    standings.push({
      teamName: roster_info.teamName,
      wins: roster_info.wins,
      losses: roster_info.losses,
      pointsFor: roster_info.pointsFor,
      pointsAgainst: roster_info.pointsAgainst,
      rank: roster_info.rank,
      strengthOfSchedule: strengthOfSchedule,
      // injuryReport: injuryReport,
      playerStats: playerStats,
    });
  });

  let output = {};
  output.leagueName = leagueData.name;
  output.numberOfTeams = rosterData.length;
  output.scoringSystem = leagueData.settings;
  output.tradeTransactions = tradeTransactions;

  output.standings = standings;

  // let save = [];

  // rosterTeam.forEach((roster) => {
  //   if (!isEmpty(roster["owner_id"])) {
  //     save.push([roster["owner_id"], roster["roster_id"]]);
  //   }
  // });

  // let roster_owner = Object.fromEntries(save);

  // let roster_summary = [];
  // rosterTeam.forEach((team) => {
  //   roster_summary.push(get_team_source(team, owner_team_name, player_name));
  // });

  let prefix = `Task: Hello, ChatGPT! Today, you are an experienced fantasy football analyst and trade transactions specialist. Using the latest JSON object data, your task is to create a captivating dialogue that dissects recent trade transactions in the fantasy football league. Start with the most recent trade transactions. Discuss the traded players, which teams they were traded from and to, and how this could affect their performance. Consider why certain teams decided to trade their players. Did they need to fill in gaps caused by injuries, or were they trying to improve a weak position? Review the impact of these trades on the league standings. How might the trades impact the teams' rankings? Share your insights on the trades. Were there any unexpected moves? Which teams do you think got the best deal, and why? Discuss the performance of the traded players in their new teams. Have they started shining, or are they struggling to fit in? Conclude with some predictions. How do you think these trades might influence the future matches? Remember, your analysis should sound like an engaging chat. It's time to jump into the game, ChatGPT! Let's make each trade transactions analysis a thrilling experience.`;
  let description = `${prefix}\n\n${JSON.stringify(output)}`;

  let gptContext = await completionContrller.generateCompletion(description);
  // let date = new Date();
  // let startDate = date.toISOString().split("T")[0];
  // let endDate = date.setDate(date.getDate() + 1);
  // endDate = new Date(endDate).toISOString().split("T")[0];

  // console.log(new Date(startDate), new Date(endDate));

  // let isReport = await ReportLog.aggregate([
  //   {
  //     $match: {
  //       run_date: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(isReport);
  let data = {
    league_id: league,
    report_id: reportId,
    user_id: decoded.id,
    content: gptContext,
    is_run: true,
    is_schedule: false,
    run_date: new Date(),
    schedule_date: null,
    recipient: recipient,
  };
  let savedReportLog = await ReportLog.create(data);
  let reportData = await Report.findById(reportId);
  await audioController.generateFile(voiceId, gptContext, savedReportLog._id);

  return {
    id: savedReportLog._id,
    name: reportData.name,
    content: gptContext,
  };
};

const generateTeamNewsAnalysis = async (req) => {
  let { league, team, reportId, recipient, voiceId, decoded } = req;
  const sleeper_instance = new sleeper_package.sleeper();
  let rotoballer_player_news = [];

  let playerData = [];
  if (await Player.count()) {
    playerData = await Player.find();
  } else {
    const fetch_player = await sleeper_instance.players.fetch_all_players();
    let player_value = fetch_player.all_players;
    playerData = Object.keys(player_value).map((key) => {
      return { ...player_value[key] };
    });
    Player.insertMany(playerData);
  }

  const league_ids = [league];
  sleeper_instance.leagues = league_ids;

  let leagueData = {};
  if (await League.count()) {
    leagueData = await League.findOne({ league_id: league });
  } else {
    const fetch_leagues = await Promise.all(sleeper_instance.league_promises);

    leagueData = fetch_leagues[0];
    League.create(leagueData);
  }

  let userData = [];
  if (await League_User.count()) {
    userData = await League_User.find({ league_id: league });
  } else {
    const fetch_users = await sleeper_instance.leagues[
      leagueData.league_id
    ].fetch_owners();

    userData = fetch_users.owners;
    League_User.insertMany(userData);
  }

  if (decoded.user_role === 1) {
    let teamUser = userData.filter(
      (user) => user.display_name === decoded.sleeper_id
    )[0];
    team = teamUser.user_id;
  }

  let rosterTeam = await League_Roster.findOne({
    league_id: league,
    owner_id: team,
  });

  let matchupData = [];
  if (await League_Matchups.count()) {
    matchupData = await League_Matchups.find({
      league_id: league,
    });
  }

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);

  let date = new Date();
  let startDate = date.setDate(date.getDate() - 7);
  startDate = new Date(startDate).toISOString().split("T")[0];
  let endDate = new Date().toISOString().split("T")[0];

  let newsData = await Player_News.aggregate([
    {
      $sort: {
        pubDate: -1,
      },
    },
    {
      $match: {
        pubDate: {
          $gte: new Date(startDate),
          $lt: new Date(endDate),
        },
      },
    },
  ]);

  console.log(startDate, endDate, newsData.length);

  console.log(startDate, endDate);

  let news_summary = [];

  let player_ids = rosterTeam.players;
  let playerNames = player_ids?.map((player_id) => player_name[player_id]);

  newsData.map((player) => {
    player.content = html_to_text(player.content);
  });

  newsData.forEach((news) => {
    let players = news?.players;
    players?.forEach((player) => {
      if (playerNames.includes(`${player.firstName} ${player.lastName}`)) {
        let { id, title, pubDate, categories, players, content } = news;
        news_summary.push({ id, title, pubDate, categories, players, content });
        console.log(news);
      }
    });
  });

  const uniqueIds = new Set(news_summary.map((obj) => obj.id));

  news_summary = news_summary.filter((obj) => {
    if (uniqueIds.has(obj.id)) {
      uniqueIds.delete(obj.id);
      return true;
    }
  });

  let output = {};
  const { wins, losses } = rosterTeam.settings;
  output.team_name = owner_team_name[rosterTeam.owner_id];
  output.wins = wins;
  output.losses = losses;
  output.starters = rosterTeam.starters?.map((item) => player_name[item]);
  output.reserve = rosterTeam.reserve?.map((item) => player_name[item]);
  output.players = rosterTeam.players?.map((item) => player_name[item]);
  output.roster_id = rosterTeam.roster_id;

  await fetch(process.env.PLAYER_NEWS_API)
    .then((response) => response.json())
    .then((data) => {
      output.players.forEach((out_playername) => {
        data.forEach((item) => {
          if (item.title.includes(out_playername)) {
            rotoballer_player_news.push(item);
          }
        });
      });
    })
    .catch((error) => console.error(error));
  output.player_news = rotoballer_player_news;

  let prefix = `Hey there, ChatGPT! Welcome to the thrilling sphere of Fantasy Football. In your role as an expert news anchor, you're tasked with interpreting and presenting the latest, most important developments from the world of Fantasy Football. Your news feed is the NFL data, given in JSON format, which you'll use to provide an engaging and conversational breaking news update. The narrative should be timely, spontaneous, and genuine—much like a live news broadcast that keeps Fantasy Football enthusiasts on the edge of their seats. Let's dive into the big headlines, key player performances, and emerging strategies that are shaking up the game this week Ready to kick off, ChatGPT? Let's make every report a fresh and unique experience!"

  Data:
  ${output}
  `;

  let description = `${prefix}\n\n${JSON.stringify(output)}`;

  let gptContext = await completionContrller.generateCompletion(description);
  // let gptContext = "GenerateTeamAnalysis";
  // let date = new Date();
  // let startDate = date.toISOString().split("T")[0];
  // let endDate = date.setDate(date.getDate() + 1);
  // endDate = new Date(endDate).toISOString().split("T")[0];

  // console.log(new Date(startDate), new Date(endDate));

  // let isReport = await ReportLog.aggregate([
  //   {
  //     $match: {
  //       run_date: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(isReport);
  let data = {
    league_id: league,
    report_id: reportId,
    user_id: decoded.id,
    content: gptContext,
    is_run: true,
    is_schedule: false,
    run_date: new Date(),
    schedule_date: null,
    recipient: recipient,
  };
  let savedReportLog = await ReportLog.create(data);
  let reportData = await Report.findById(reportId);
  await audioController.generateFile(voiceId, gptContext, savedReportLog._id);

  return {
    id: savedReportLog._id,
    name: reportData.name,
    content: gptContext,
  };
};

const generateWWAnalysis = async (req) => {};

module.exports = {
  generatePreMatchupAnalysis,
  generatePostMatchupAnalysis,
  generatePowerRanking,
  generateTeamAnalysis,
  generateTransactionAnalysis,
  generateTeamNewsAnalysis,
  generateWWAnalysis,
};
