const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_User = require("../models/LeagueUser.model");
// const LeaguePlayers = require("../models/LeaguePlayers.model");
const Player_News = require("../models/PlayerNews.model");
const axios = require("axios");
const isEmpty = require("is-empty");
const moment = require("moment");
const {
  html_to_text,
  get_player_news,
  convert_player_name,
  convert_owner_team_name,
} = require("../utils/report");

/**
 * Generate team news prompt
 */
const generate_prompt = async (req, res) => {
  console.log(req.body);

  let playerData = await Player.find();

  let leagueData = await League.findOne({ league_id: req.body.league });

  let rosterData = await League_Roster.findOne({
    league_id: req.body.league,
    owner_id: req.body.team,
  });

  let userData = await League_User.find({ league_id: req.body.league });

  // let date = new Date();
  // let startDate = new Date(date.setMonth(date.getMonth() - 1));
  // let endDate = new Date();

  // console.log(startDate, endDate);

  // let player_news_data = await Player_News.aggregate([
  //   {
  //     $match: {
  //       pubDate: {
  //         $gte: startDate,
  //         $lte: endDate,
  //       },
  //     },
  //   },
  // ]);

  // console.log(player_news_data.length);

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);

  // let league_players = await LeaguePlayers.find();

  // if (isEmpty(league_players)) {
  //   let url = `https://api.sportsdata.io/v3/nfl/scores/json/Players?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //   await axios(url)
  //     .then((players) => {
  //       LeaguePlayers.create(players.data);
  //       league_players = players.data;
  //     })
  //     .catch((err) => console.log(err));
  // }

  let news_summary = [];

  let player_ids = rosterData.players;
  // let sports_ids = player_ids.map((player) => {
  //   return playerData.filter((item) => item.player_id === player)[0]
  //     ?.sportradar_id;
  // });

  // let sports_players_ids = [];
  // sports_ids.forEach((radar) => {
  //   if (!isEmpty(radar)) {
  //     sports_players_ids.push(
  //       league_players.filter((item) => item.SportRadarPlayerID === radar)[0]
  //         ?.PlayerID
  //     );
  //   }
  // });

  // await Promise.all(
  //   player_ids.map(async (playerId) => {
  //     console.log(playerId);
  //     let url = `https://api.sportsdata.io/v3/nfl/news-rotoballer/json/RotoBallerPremiumNewsByPlayerID/${playerId}?key=d90e49f6d6bf4645ba07a747c7e47a07`;
  //     await axios(url)
  //       .then((res) => {
  //         console.log(res.data[0]);
  //         let data = res.data[0];
  //         let player_data = league_players.filter(
  //           (item) => item.PlayerID === playerId
  //         )[0];
  //         let player_info = {
  //           firstName: player_data.FirstName,
  //           lastName: player_data.LastName,
  //           position: player_data.Position,
  //         };
  //         news_summary.push({
  //           id: playerId,
  //           title: data.Title,
  //           pubDate: data.Updated,
  //           categories: data.Categories,
  //           players: player_info,
  //           content: data.Content,
  //         });
  //       })
  //       .catch((err) => console.log(err));
  //   })
  // );
  let output = {};
  const { wins, losses } = rosterData.settings;
  output.team_name = owner_team_name[rosterData.owner_id];
  output.wins = wins;
  output.losses = losses;
  output.starters = rosterData.starters?.map((item) => player_name[item]);
  output.reserve = rosterData.reserve?.map((item) => player_name[item]);
  output.players = rosterData.players?.map((item) => player_name[item]);
  output.roster_id = rosterData.roster_id;
  output.player_news = news_summary;

  let prefix = `Hey there, ChatGPT! Welcome to the thrilling sphere of Fantasy Football. In your role as an expert news anchor, you're tasked with interpreting and presenting the latest, most important developments from the world of Fantasy Football. Your news feed is the NFL data, given in JSON format, which you'll use to provide an engaging and conversational breaking news update. The narrative should be timely, spontaneous, and genuine—much like a live news broadcast that keeps Fantasy Football enthusiasts on the edge of their seats. Let's dive into the big headlines, key player performances, and emerging strategies that are shaking up the game this week!`;

  let date = new Date();
  let startDate = date.setDate(date.getDate() - 7);
  startDate = new Date(startDate).toISOString().split("T")[0];
  let endDate = date.setDate(date.getDate() + 1);
  endDate = new Date(endDate).toISOString().split("T")[0];
  let newsData = await Player_News.aggregate([
    {
      $match: {
        pubDate: {
          $gte: new Date(startDate),
          $lt: new Date(endDate),
        },
      },
    },
  ]);

  console.log(startDate, endDate);

  res.send({ prefix, output });
};

/**
 * Get new data by day
 */
const get_news_data = async (req, res) => {
  // let { end } = req.body;
  console.log(req.body);
  // let date = new Date();
  // let startDate = date.setDate(date.getDate() - 7);
  // startDate = new Date(startDate).toISOString().split("T")[0];
  // let endDate = new Date().toISOString().split("T")[0];
  // let newsData = await Player_News.aggregate([
  //   {
  //     $match: {
  //       pubDate: {
  //         $gte: new Date(startDate),
  //         $lt: new Date(endDate),
  //       },
  //     },
  //   },
  // ]);

  // console.log(startDate, endDate, newsData.length);

  try {
    for (step = 1; step <= 7; step++) {
      let startDate = moment(
        new Date().setDate(new Date().getDate() - step)
      ).format("YYYYMMDD");
      let endDate = moment(
        new Date().setDate(new Date().getDate() - step)
      ).format("YYYYMMDD");
      console.log(startDate, endDate);

      let url = `https://www.rotoballer.com/partner-rssxml?partner=ardent_principles&key=3hR.p.K9Q5CTi.Mj9sQRbY6VsGszQ6YDalCunCZdBGU1AM97CQK1vZ1DIpUk&sport=nfl&format=json&dateStart=${startDate}&dateStop=${endDate}`;

      axios(url)
        .then(async (res) => {
          // let news_ids = await Player_News.find();
          // news_ids = news_ids.map((item) => item.id);
          let insertIds = res.data.map((item) => item.id);
          // let save = [];
          // res.data.map((news) => {
          //   if (!news_ids.includes(news.id)) {
          //     save.push(news);
          //   }
          // });
          console.log(startDate, res.data.length);

          await Player_News.deleteMany({ id: { $in: insertIds } });
          await Player_News.insertMany(res.data);
          // Player_News.distinct("id", (error, ids) => {
          //   console.log(error, ids);
          // });
        })
        .catch((err) => console.log(err));
    }
    res.send("ok");
  } catch (err) {
    console.log(err);
  }
};

/**
 * Delete news data
 */
const delete_news_data = async (req, res) => {
  await Player_News.deleteMany({});
  let test = await Player_News.find();
  console.log(test.length);
};

module.exports = {
  generate_prompt,
  get_news_data,
  delete_news_data,
};
