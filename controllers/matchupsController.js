const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_Matchups = require("../models/LeagueMatchups.model");
const League_User = require("../models/LeagueUser.model");
const WeekProjections = require("../models/WeekProjections.model");
const WeekStats = require("../models/WeekStats.model");
const ReportLog = require("../models/ReportLog.model");
const JWT = require("jsonwebtoken");
const sleeper_package = require("sleeper_fantasy");
const axios = require("axios");
const {
  convert_player_name,
  convert_owner_team_name,
  convert_roster_team_name,
  webScraping,
  get_standings,
  map_users_to_team_name,
} = require("../utils/report");
const isEmpty = require("is-empty");

const makePreMatchup = async () => {};

const makePostMatchup = async () => {};

const get_team = async (req, res) => {
  // let teams_list = await Teams.find();
  // let output = teams_list.map((team) => {
  //   return { Key: team.Key, FullName: team.FullName };
  // });
  // res.send(output);
  let userData = [];
  let user_count = await League_User.count({ league_id: req.body.league });
  if (user_count) {
    userData = await League_User.find({ league_id: req.body.league });
  }
  let data = map_users_to_team_name(userData);
  res.send(data);
};

const getMatchupByLeague = async (req, res) => {
  console.log(req.body);

  let rosterData = await League_Roster.find({ league_id: req.body.league });

  let userData = await League_User.find({ league_id: req.body.league });

  let matchup_data = await League_Matchups.findOne({
    league_id: req.body.league,
    week: req.body.week,
  });

  let owner_team_name = convert_owner_team_name(userData);
  let roster_team_name = convert_roster_team_name(rosterData, owner_team_name);

  let matchup_team = {};
  matchup_data?.matchups.forEach((team) => {
    let matchup_id = team.matchup_id;

    if (isEmpty(matchup_team[matchup_id])) {
      matchup_team[matchup_id] = [roster_team_name[team.roster_id]];
    } else {
      matchup_team[matchup_id].push(roster_team_name[team.roster_id]);
    }
  });

  res.send(matchup_team);
};

module.exports = {
  get_team,
  getMatchupByLeague,
  makePreMatchup,
  makePostMatchup,
};
