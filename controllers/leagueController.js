const sleeper_package = require("sleeper_fantasy");
const League_User = require("../models/LeagueUser.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const JWT = require("jsonwebtoken");

const { Success, ServerError, NotFound } = require("../constants/StatusCode");

const { ServerErrorMsg } = require("../constants/StatusMessage");

/**
 * Get league list by user_id
 */
const getLeagueByUser = async (req, res) => {
  try {
    const sleeper_instance = new sleeper_package.sleeper();
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { sleeper_id, id, user_role } = decoded;

    let leagueList = [];
    let season = new Date().getFullYear();
    // console.log(typeof season);
    // let leagueData = await League.find({ season: season.toString() });

    // let leagueInfo = [];
    // leagueData.forEach((item) => {
    //   leagueInfo.push({ league_id: item.league_id });
    // });

    // console.log(leagueInfo);
    if (user_role === 1) {
      const usernames = [sleeper_id];
      sleeper_instance.users = usernames;
      await Promise.all(sleeper_instance.user_promises);
      const user_leagues = await sleeper_instance.users[
        sleeper_id
      ].fetch_leagues(season);
      leagueList = user_leagues?.leagues;

      await Promise.all(
        leagueList.map(async (leagueData, idx) => {
          await League.deleteOne({ league_id: leagueData["league_id"] });
          await League.create(leagueData);
        })
      );
    } else {
      leagueList = await League_User.aggregate([
        {
          $match: {
            display_name: sleeper_id,
            $or: leagueInfo,
          },
        },
        {
          $lookup: {
            from: "leagues",
            localField: "league_id",
            foreignField: "league_id",
            as: "league",
          },
        },
        { $unwind: "$league" },
        // {
        //   $group: {
        //     _id: {
        //       league_id: 1,
        //       name: "$league.name",
        //       season: "$league.season"
        //     },
        //   },
        // },
        // {
        //   $project: {
        //     _id: 0, // Optional: remove Mongo's default _id field from the result
        //     league_id: "$league.league_id",
        //     name: "$league.name",
        //     season: "$league.season",
        //     // Add more fields as needed from the "leagues" collection
        //   },
        // },
      ]);
    }

    // Check if the leagueList is not empty
    if (leagueList.length > 0) {
      res.status(Success).json({ list: leagueList, message: "Success!" });
    } else {
      res
        .status(NotFound)
        .json({ liest: leagueList, message: "No leagues found." });
    }
  } catch (error) {
    console.error(error); // Log the error for debugging purposes
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const updateLeague = async (req, res) => {
  try {
    const { id } = req.params;
    let data = req.body;
    console.log(req.body);
    // console.log(typeof req.body.start_date);
    // data.start_date = new Date(data.start_date);
    console.log(data);
    const updatedLeague = await League.findOneAndUpdate(
      { league_id: id },
      data,
      {
        new: true,
      }
    );
    if (updatedLeague) {
      res
        .status(Success)
        .json({ league: updatedLeague, message: "Update successful" });
    } else {
      res.status(NotFound).json({ message: "League not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

module.exports = {
  getLeagueByUser,
  updateLeague,
};
