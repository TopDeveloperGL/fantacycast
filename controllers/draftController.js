const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_User = require("../models/LeagueUser.model");
// const Player_Stats = require("../models/player_stats.model");
const DraftPicks = require("../models/DraftPicks.model");
const Draft = require("../models/Draft.model");
const sleeper_package = require("sleeper_fantasy");
const isEmpty = require("is-empty");
const {
  convert_player_name,
  convert_owner_team_name,
} = require("../utils/report");

const seperateByOwner = (pickData, playerIds) => {
  let save = [];
  pickData.forEach((item) => {
    if (playerIds.includes(item.player_id)) {
      save.push({
        round: item.round,
        pick_no: item.pick_no,
        player_metadata: {
          status: item.metadata?.status,
          position: item.metadata?.position,
          player_id: item.metadata?.player_id,
          number: item.metadata?.number,
          last_name: item.metadata?.last_name,
          injury_status: item.metadata?.injury_status,
          first_name: item.metadata?.first_name,
        },
        is_keeper: item.is_keeper,
      });
    }
  });
  return save;
};

const draftByTeam = async (req, res) => {
  console.log(req.body);
  const sleeper_instance = new sleeper_package.sleeper();
  let playerData = await Player.find();

  let leagueData = await League.findOne({ league_id: req.body.league });

  let rosterData = await League_Roster.find({ league_id: req.body.league });

  let rosterTeam = await League_Roster.findOne({
    league_id: req.body.league,
    owner_id: req.body.team,
  });

  let userData = await League_User.find({ league_id: req.body.league });

  const draft_ids = [leagueData.draft_id];
  sleeper_instance.drafts = draft_ids;

  let draftData = await Draft.findOne({
    draft_id: leagueData.draft_id,
  });

  if (isEmpty(draftData)) {
    const fetchDraft = await Promise.all(sleeper_instance.draft_promises);
    Draft.create(fetchDraft[0]);
    draftData = fetchDraft[0];
  }

  let draftByLeague = await DraftPicks.findOne({
    league_id: leagueData.league_id,
  });

  let draftPicksData = draftByLeague?.picks;

  if (isEmpty(draftPicksData)) {
    const fetchDraftPicks = await sleeper_instance.drafts[
      leagueData.draft_id
    ].picks();

    DraftPicks.create({
      league_id: leagueData.league_id,
      picks: fetchDraftPicks._picks,
    });

    draftPicksData = fetchDraftPicks._picks;
  }

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);

  let picks = seperateByOwner(draftPicksData, rosterTeam.players);
  let output = {};
  output.type = draftData.type;
  output.status = draftData.status;
  output.sport = draftData.sport;
  output.settings = {
    teams: draftData.settings?.teams,
    slots_wr: draftData.settings?.slots_wr,
    slots_te: draftData.settings?.slots_te,
    slots_rb: draftData.settings?.slots_rb,
    slots_qb: draftData.settings?.slots_qb,
    slots_flex: draftData.settings?.slots_flex,
    slots_def: draftData.settings?.slots_def,
    slots_bn: draftData.settings?.slots_bn,
    rounds: draftData.settings?.rounds,
  };
  output.season_type = draftData.season_type;
  output.season = draftData.season;
  output.metadata = {
    scoring_type: draftData.metadata?.scoring_type,
    name: draftData.metadata?.name,
    description: draftData.metadata?.description,
  };
  output.league_id = draftData.league_id;
  output.draft_order = draftData.draft_order;
  output.draft_id = draftData.draft_id;
  output.picks = picks;

  let prefix = `Hey there, ChatGPT! Time to put on our draft analyst hat and dive deep into the world of Fantasy Football drafts. Your role is akin to a seasoned scout, using the league's draft data, provided in JSON format, to craft a comprehensive draft analysis. The discussion should be as engaging as a table talk between a group of fantasy managers right after a draft.Why not begin with some shockers from the draft? Perhaps there's a team that went against the conventional wisdom or a sleeper pick that raised eyebrows. Given the draft order, who seemed to have the best strategy and who gambled? Delve into the player metadata. Highlight their positions, expected vs. actual draft position, or any injury status that might have influenced the pick.Were there players who were picked way earlier than their average draft positions? Or others who were surprisingly overlooked until the later rounds? Reflect upon their recent seasons, team moves, or even their projected season points. Were certain positions more favored in this draft than others, like QBs or WRs?A touch of nostalgia wouldn’t hurt. Recall any historical performances by the drafted players, any standout seasons, or memorable matchups. Then, pivot to the implications of this draft. Offer insights on potential starting lineups, bench depth, or highlight players who, even if drafted late, might turn into gold during the season.Sprinkle the chat with interesting tidbits, light-hearted jabs, and maybe a few bold predictions for the season. Use lucid and engaging language, and remember the rounding rule for decimals. If you see averages or projections with decimals of .5 or higher, round them up.Conclude with your overall impressions, standout teams post-draft, and perhaps some early projections for the season's top performers. Employ varied sentence structures and transition words to ensure your analysis is fluid and captivating.`;

  res.send({ prefix, output });

  // let prefix = `Greetings, ChatGPT. Your task is to deliver an all-encompassing analysis for this week's fantasy football league.\n
  // As an esteemed sports commentator, your voice will resonate through the realm of fantasy football, delivering riveting live analysis. Our craft isn't merely dealing with dry numbers – it's about spinning captivating stories. Weave a narrative that dives into the dynamics of the league, resonating with every participant, from the wide-eyed rookies to the grizzled veterans.\n
  // Create a seamless narrative integrating Drafts Data without explicit divisions. Don't forget the historical context – have these teams locked horns in the past? What was the outcome? Offer a glimpse into their storied rivalry, their past victories and defeats, painting a narrative that transcends this week's game.\n
  // 1. **Story-Driven Analysis**: Weave Drafts Data into a captivating story rather than a simple presentation of facts. Each piece of information becomes part of a grand narrative, creating a unique and fresh perspective for each matchup.\n
  // 2. **Detailed Player Analysis**: Go beyond the projections. Give life to the numbers. What is the reasoning behind Aaron Rodgers' high projection this week? Is it due to his recent performances? Or maybe his opponent's defense has been lacking? Deep dive into these intricacies to offer our audience a richer understanding of the game dynamics.\n
  // 3. **Strategic Suggestions**: In your team analysis, don't just highlight potential pitfalls, offer solutions. What can a team do to address a struggling defense? Should they change their lineup? Are there some underutilized players that could turn the tide? This strategic advice will not only enrich your analysis but also provide the crucial insights our league participants crave.\n
  // 4. **Interactive Discussion**: Engage the audience throughout your analysis. Pose questions, invite predictions, spark lively debates. Keep the conversation flowing and let their excitement build up to fever pitch as we approach the next round of games.\n
  // 5. **Concluding Thoughts and Bold Predictions**: Bring it all together in a grand finale. What are your key takeaways? Offer specific strategic advice and make some audacious predictions based on your comprehensive analysis. Remember, in the ever-evolving world of fantasy football, your informed projections can be the guiding light for our participants.\n
  // As our trusted guide, navigate our fantasy football league participants through the tumultuous seas of this week's games. Deliver the knowledge they need to make the winning decisions, weaving a narrative that keeps them enthralled. The stage is set. We await your analysis with bated breath.\n`;
};

module.exports = {
  draftByTeam,
};
