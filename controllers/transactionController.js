const Player = require("../models/Player.model");
const League = require("../models/Leauge.model");
const League_Roster = require("../models/LeagueRoster.model");
const League_Matchup = require("../models/LeagueMatchups.model");
const League_User = require("../models/LeagueUser.model");
const WeekTransactions = require("../models/WeekTransactions.model");
const Injuries = require("../models/Injuries.model");
const PlayerStats = require("../models/SeasonStats.model");
const PlayerProjections = require("../models/SeasonProjections.model");
const axios = require("axios");
const isEmpty = require("is-empty");
const {
  convert_player_name,
  convert_owner_team_name,
  convert_roster_team_name,
  map_users_to_team_name,
  clamp,
} = require("../utils/report");

const get_transaction_text = (
  transactions,
  player_data,
  player_name,
  roster_name,
  roster_id
) => {
  let summary = [];
  transactions?.forEach((transaction, idx) => {
    if (transaction.status === "complete") {
      summary.push(`${idx + 1}. Transaction`);
      if (transaction.type === "trade") {
        let tradeData = Object.entries(transaction["adds"]);
        let nowMember = [];
        tradeData.map((item) => {
          if (item[1] === roster_id)
            nowMember.push(
              `${player_name[item[0]]} (${player_data[item[0]]["position"]})`
            );
        });
        let tradeMember = [];
        tradeData.map((item) => {
          if (item[1] !== roster_id)
            tradeMember.push(
              `${player_name[item[0]]} (${player_data[item[0]]["position"]})`
            );
        });

        summary.push(
          `The ${roster_name[roster_id]} team trade ${nowMember.join(
            ", "
          )} for ${tradeMember.join(", ")}.`
        );
      } else {
        if (!isEmpty(transaction["drops"])) {
          let dropData = Object.entries(transaction["drops"]);

          summary.push(
            `The ${roster_name[roster_id]} team drops ${dropData
              .map((item) => {
                return `${player_name[item[0]]} (${
                  player_data[item[0]]["position"]
                })`;
              })
              .join(", ")}.`
          );
        }
        if (!isEmpty(transaction["adds"])) {
          let addData = Object.entries(transaction["adds"]);

          summary.push(
            `The ${roster_name[roster_id]} team adds ${addData
              .map((item) => {
                return `${player_name[item[0]]} (${
                  player_data[item[0]]["position"]
                })`;
              })
              .join(", ")}.`
          );
        }
      }
    }
  });
  if (!isEmpty(transactions)) return summary.join("\n");
  else return "Empty";
};

const get_all_transaction_text = (
  transactions,
  player_data,
  player_name,
  roster_name,
  roster_owner
) => {
  let summary = [];
  let idx = 1;
  transactions.forEach((transaction) => {
    let roster_id = roster_owner[transaction["creator"]];
    if (transaction.status === "complete") {
      if (transaction.type === "trade") {
        let tradeData = Object.entries(transaction["adds"]);
        let nowMember = [];
        tradeData.map((item) => {
          if (item[1] === roster_id)
            nowMember.push(
              `${player_name[item[0]]} (${player_data[item[0]]["position"]})`
            );
        });
        let tradeMember = [];
        tradeData.map((item) => {
          if (item[1] !== roster_id)
            tradeMember.push(
              `${player_name[item[0]]} (${player_data[item[0]]["position"]})`
            );
        });

        summary.push(
          `${idx}. The ${roster_name[roster_id]} team trade ${nowMember.join(
            ", "
          )} for ${tradeMember.join(", ")}.`
        );
      } else {
        let idx_flag = false;
        if (!isEmpty(transaction["drops"])) {
          let dropData = Object.entries(transaction["drops"]);
          idx_flag = true;

          summary.push(
            `${idx}. The ${roster_name[roster_id]} team drops ${dropData
              .map((item) => {
                return `${player_name[item[0]]} (${
                  player_data[item[0]]["position"]
                })`;
              })
              .join(", ")}.`
          );
        }
        if (!isEmpty(transaction["adds"])) {
          let addData = Object.entries(transaction["adds"]);

          summary.push(
            `${!idx_flag ? idx + ". " : ""}The ${
              roster_name[roster_id]
            } team adds ${addData
              .map((item) => {
                return `${player_name[item[0]]} (${
                  player_data[item[0]]["position"]
                })`;
              })
              .join(", ")}.`
          );
        }
      }
      idx += 1;
    }
  });
  if (summary.length) return summary.join("\n");
  else return "Empty";
};

const get_standings = (rosters, roster_name) => {
  let roster_standings_list = [];
  let rank_list = {};

  rosters.forEach((item) => {
    let wins = item.settings.wins;
    let losses = item.settings.losses;
    let ties = item.settings.ties;
    let points = item.settings.fpts;
    let points_against = item.settings.fpts_against;
    let name = roster_name[item.roster_id];

    roster_standings_list.push({
      roster_id: item.roster_id,
      teamName: name,
      wins: wins,
      losses: losses,
      pointsFor: points,
      pointsAgainst: points_against,
      percent:
        (wins / (wins + losses + ties)) * 0.5 +
        (points / (points + points_against)) * 0.5,
    });
  });

  roster_standings_list.sort((a, b) => b.percent - a.percent);
  roster_standings_list = roster_standings_list.map((item, idx) => {
    return { ...item, rank: idx + 1 };
  });

  return roster_standings_list;
};

const get_team_source = (team, owner_team_name, player_name) => {
  let players = team["players"];
  let starters = team["starters"];
  let bench = players.filter((player) => !starters.includes(player));
  let reserve = !isEmpty(team["reserve"]) ? team["reserve"] : [];
  let keepers = !isEmpty(team["keepers"]) ? team["keepers"] : [];

  let summary = [];

  summary.push(`Team Name: ${owner_team_name[team["owner_id"]]}`);
  summary.push(
    `Starters: ${starters.map((item) => `${player_name[item]}`).join(", ")}`
  );
  summary.push(
    `Bench: ${bench.map((item) => `${player_name[item]}`).join(", ")}`
  );

  summary.push(
    `Reserve: ${reserve.map((item) => `${player_name[item]}`).join(", ")}`
  );
  summary.push(
    `Keepers: ${keepers.map((item) => `${player_name[item]}`).join(", ")}`
  );

  return summary.join("\n");
};

const trade_analysis = (transaction, player_name, player_data, roster_name) => {
  let { type, status, roster_ids, drops, adds, created } = transaction;

  let trans = {};
  let adds_data = Object.entries(adds);
  let drops_data = Object.entries(drops);

  let change_adds = adds_data.map((add) => {
    return [player_name[add[0]], roster_name[add[1]]];
  });

  let change_drops = drops_data.map((add) => {
    return [player_name[add[0]], roster_name[add[1]]];
  });

  trans.tradeDate = created;
  trans.drops = Object.fromEntries(change_drops);
  trans.adds = Object.fromEntries(change_adds);
  trans.playersInvolved = [];

  drops_data.map((drop) => {
    // trans.tradedFrom = drop[1];
    // trans.tradedTo = adds_data.filter(add => add[0] === drop[0])[0][1];
    trans.playersInvolved.push({
      playerName: player_name[drop[0]],
      position: player_data[drop[0]].position,
    });
  });

  return trans;
};

/**
 * Get opponents list
 * @param {Array} matchups
 * @param {int} roster_id
 * @returns
 */
const get_opponents = (matchups, roster_id) => {
  let opponents_list = [];

  matchups.forEach((item) => {
    let team_matchups = item.matchups;

    // Get target team info by roster_id
    let target_team = team_matchups.filter(
      (matchup) => matchup.roster_id === roster_id
    )[0];

    // Get opponent team info by matchup_id
    let opponent_team = team_matchups.filter(
      (matchup) => matchup.matchup_id === target_team.matchup_id
    )[0];

    if (!opponents_list.includes(opponent_team.roster_id)) {
      opponents_list.push(opponent_team.roster_id);
    }
  });

  console.log(opponents_list);

  return opponents_list;
};

/**
 * Calculate strength of schedule
 * @param {Array} standings
 * @param {Int} team_name
 * @param {Array} opponents
 * @returns
 */
const calculate_strength_of_schedule = (standings, team_name, opponents) => {
  // Get the list of teams the input team has played against
  // let opponents = get_opponents(team_name);

  let total_wins = 0;
  let total_losses = 0;
  let total_points = 0;
  let total_games = 0;

  let min_points = 1000;
  let max_points = 0;
  // Sum up the total wins, total losses and total points of the opponents
  standings.forEach((team) => {
    if (opponents.includes(team.roster_id)) {
      total_wins += team.wins;
      total_losses += team.losses;
      total_points += team.pointsFor;
      total_games += team.wins + team.losses;

      if (min_points > team.pointsFor / (team.wins + team.losses)) {
        min_points = team.pointsFor / (team.wins + team.losses);
      }

      if (max_points < team.pointsFor / (team.wins + team.losses)) {
        max_points = team.pointsFor / (team.wins + team.losses);
      }
    }
  });

  // Calculate win-loss ratio
  let win_loss_ratio = total_wins / total_games;

  // Calculate average points scored by opponents
  let average_points_scored = total_points / total_games;

  // Assume we have a function to normalize these values so they're on the same scale
  let normalized_win_loss_ratio = normalize_win_loss_ratio(win_loss_ratio);
  let normalized_points_scored = normalize_points_scored(
    average_points_scored,
    min_points,
    max_points
  );

  // Calculate and return the strength of schedule as the average of the normalized values
  let strength_of_schedule =
    (normalized_win_loss_ratio + normalized_points_scored) / 2;

  return strength_of_schedule;
};

/**
 * Normalize win_loss_ratio
 * @param {float} win_loss_ratio
 * @returns
 */
const normalize_win_loss_ratio = (win_loss_ratio) => {
  // Since win_loss_ratio is already a value between 0 and 1, no normalization might be needed.
  // But just in case values are not strictly between 0 and 1, we can add a safety clamp.
  return clamp(win_loss_ratio, 0, 1);
};

/**
 * Normalize points_scored
 * @param {float} points
 * @param {float} min_points
 * @param {float} max_points
 * @returns
 */
const normalize_points_scored = (points, min_points, max_points) => {
  // Subtract the minimum number of points from the input points
  let normalized_points = points - min_points;
  // Divide by the range of possible points
  normalized_points = normalized_points / (max_points - min_points);
  return normalized_points;
};

const get_transaction = async (req, res) => {
  let playerData = await Player.find();

  let leagueData = await League.findOne({ league_id: req.body.league });

  // let rosterData = {};
  // if (req.body.team !== "All") {
  //   let roster_count = await League_Roster.count({
  //     league_id: req.body.league,
  //     owner_id: req.body.team,
  //   });
  //   if (roster_count) {
  //     rosterData = await League_Roster.findOne({
  //       league_id: req.body.league,
  //       owner_id: req.body.team,
  //     });
  //   }
  // }

  let matchup_data = await League_Matchup.find({
    league_id: req.body.league,
  });

  let rosterTeam = await League_Roster.find({
    league_id: req.body.league,
  });

  let userData = await League_User.find({
    league_id: req.body.league,
  });

  let week_info = [];
  req.body.weeks?.map((item) => {
    week_info.push({ week: item[0] });
  });

  let transaction_database = await WeekTransactions.aggregate([
    {
      $match: { league_id: req.body.league },
    },
    {
      $match: {
        $or: week_info,
      },
    },
  ]);

  // let injuries_weeks = [];
  // await Promise.all(
  //   req.body.weeks?.map(async (item) => {
  //     let injuries_by_week = await Injuries.findOne({
  //       season: leagueData.season,
  //       season_type: 1,
  //       week: item[0],
  //     });

  //     let injuries_team = injuries_by_week?.data;

  //     if (isEmpty(injuries_by_week)) {
  //       let url = `https://api.sportsdata.io/v3/nfl/stats/json/Injuries/${leagueData.season}/${item[0]}?key=f3fda91edfd8499a8bb4e72a84c72e53`;
  //       await axios(url)
  //         .then((injuries) => {
  //           Injuries.create({
  //             season: leagueData.season,
  //             season_type: 1,
  //             week: item[0],
  //             data: injuries.data,
  //           });
  //           injuries_team = injuries.data;
  //         })
  //         .catch((err) => console.log(err));
  //     }
  //     injuries_weeks = injuries_weeks.concat(injuries_team);
  //   })
  // );
  // console.log(injuries_weeks.length);

  // let injuries_by_weeks = await Injuries.aggregate([
  //   {
  //     $match: {
  //       season: leagueData.season,
  //       season_type: 1,
  //     },
  //   },
  //   {
  //     $match: {
  //       $or: week_info,
  //     },
  //   },
  // ]);

  let stats_by_season = await PlayerStats.findOne({
    season: leagueData.season,
    season_type: leagueData.season_type,
  });

  let stats_data = stats_by_season?.data;

  if (isEmpty(stats_by_season)) {
    let fetchStats = await sleeper_instance.players.fetch_stats(
      leagueData["season_type"],
      leagueData["season"]
    );
    PlayerStats.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      data: fetchStats.stats,
    });
    stats_data = fetchStats.stats;
  }
  // console.log(stats_data);

  let projections_by_season = await PlayerProjections.findOne({
    season: leagueData.season,
    season_type: leagueData.season_type,
  });

  let projections_data = projections_by_season?.data;

  if (isEmpty(projections_by_season)) {
    let fetchProjections = await sleeper_instance.players.fetch_projections(
      leagueData.season_type,
      leagueData.season
    );
    PlayerProjections.create({
      season: leagueData.season,
      season_type: leagueData.season_type,
      data: fetchProjections.projections,
    });

    projections_data = fetchProjections.projections;
  }

  let players = [];
  playerData.forEach((item) => {
    players.push([item.player_id, item]);
  });

  let player_name = convert_player_name(playerData);
  let owner_team_name = convert_owner_team_name(userData);
  let roster_name = convert_roster_team_name(rosterTeam, owner_team_name);

  let tradeTransactions = [];

  let roster_list = [];
  transaction_database.forEach((item) => {
    item.data?.forEach((transaction) => {
      if (transaction.status === "complete" && transaction.type === "trade") {
        transaction.roster_ids?.forEach((item) => {
          if (!roster_list.includes(item)) {
            roster_list.push(item);
          }
        });
        // console.log(transaction);
        let tran_info = trade_analysis(
          transaction,
          player_name,
          Object.fromEntries(players),
          roster_name
        );
        tradeTransactions.push(tran_info);
      }
    });
  });

  let standings_data = get_standings(rosterTeam, roster_name);

  let standings = [];
  roster_list.forEach((roster_id) => {
    let strengthOfSchedule = calculate_strength_of_schedule(
      standings_data,
      roster_id,
      get_opponents(matchup_data, roster_id)
    );

    let roster_info = standings_data.filter(
      (sub_item) => sub_item.roster_id === roster_id
    )[0];

    let players = rosterTeam.filter((team) => team.roster_id === roster_id)[0]
      .players;

    // let injuryReport = [];
    let playerStats = [];
    players.forEach((player_id) => {
      // let injury_info = injuries_weeks.filter(
      //   (injury) => injury.PlayerID === player_id
      // )[0];
      // if (!isEmpty(injury_info)) {
      //   injuryReport.push({
      //     Name: injury_info.Name,
      //     Position: injury_info.Position,
      //     BodyPart: injury_info.BodyPart,
      //     Status: injury_info.Status,
      //     Practice: injury_info.Practice,
      //     PracticeDescription: injury_info.PracticeDescription,
      //     Updated: injury_info.Updated,
      //     DeclaredInactive: injury_info.DeclaredInactive,
      //   });
      // }
      let player_info = playerData.filter(
        (item) => item.player_id === player_id
      )[0];
      let stats_info = stats_data?.player_id;
      let projections_info = projections_data?.player_id;
      if (!isEmpty(stats_info)) {
        playerStats.push({
          playerName: player_info?.full_name,
          position: player_info?.position,
          // team: stats_info.Team,
          passingYards: stats_info?.pass_yd,
          passingTDs: stats_info?.pass_td,
          // interceptions: stats_info.Interceptions,
          // receptions: stats_info.Receptions,
          receivingYards: stats_info?.rec_yd,
          receivingTDs: stats_info?.rec_td,
          SeasonProjectedPoints: projections_info?.pts_std,
        });
      }
    });

    standings.push({
      teamName: roster_info.teamName,
      wins: roster_info.wins,
      losses: roster_info.losses,
      pointsFor: roster_info.pointsFor,
      pointsAgainst: roster_info.pointsAgainst,
      rank: roster_info.rank,
      strengthOfSchedule: strengthOfSchedule,
      // injuryReport: injuryReport,
      playerStats: playerStats,
    });
  });

  let output = {};
  output.leagueName = leagueData.name;
  output.numberOfTeams = rosterTeam.length;
  output.scoringSystem = leagueData.settings;
  output.tradeTransactions = tradeTransactions;

  output.standings = standings;

  // let save = [];

  // rosterTeam.forEach((roster) => {
  //   if (!isEmpty(roster["owner_id"])) {
  //     save.push([roster["owner_id"], roster["roster_id"]]);
  //   }
  // });

  // let roster_owner = Object.fromEntries(save);

  // let roster_summary = [];
  // rosterTeam.forEach((team) => {
  //   roster_summary.push(get_team_source(team, owner_team_name, player_name));
  // });

  let prefix = `Task: Hello, ChatGPT! Today, you are an experienced fantasy football analyst and trade transactions specialist. Using the latest JSON object data, your task is to create a captivating dialogue that dissects recent trade transactions in the fantasy football league. Start with the most recent trade transactions. Discuss the traded players, which teams they were traded from and to, and how this could affect their performance. Consider why certain teams decided to trade their players. Did they need to fill in gaps caused by injuries, or were they trying to improve a weak position? Review the impact of these trades on the league standings. How might the trades impact the teams' rankings? Share your insights on the trades. Were there any unexpected moves? Which teams do you think got the best deal, and why? Discuss the performance of the traded players in their new teams. Have they started shining, or are they struggling to fit in? Conclude with some predictions. How do you think these trades might influence the future matches? Remember, your analysis should sound like an engaging chat. It's time to jump into the game, ChatGPT! Let's make each trade transactions analysis a thrilling experience.`;

  res.send({ prefix, output });
};

module.exports = { get_transaction };
