const ReportLog = require("../models/ReportLog.model");
const User = require("../models/User.model");
const JWT = require("jsonwebtoken");
const mongoose = require("mongoose");

const {
  Success,
  ServerError,
  NotFound,
  Created,
} = require("../constants/StatusCode");

const { ServerErrorMsg } = require("../constants/StatusMessage");
const { ObjectId } = require("mongodb");
const isEmpty = require("is-empty");

const findNextDay = (days, currentDay, date) => {
  const sortedDays = days.sort((a, b) => a - b);
  let nextDay;

  for (let i = 0; i < sortedDays.length; i++) {
    if (sortedDays[i] > currentDay) {
      nextDay = sortedDays[i];
      break;
    }
  }

  if (!nextDay && nextDay !== 0) {
    // if nextDay is not found, it means next day is the first day in the schedule
    nextDay = sortedDays[0];
  }

  // Calculate the difference and add to the original date
  let difference = nextDay - currentDay;
  if (difference < 0) {
    // if the difference is negative, it means the next day is in the next week
    difference += 7;
  }

  date.setDate(date.getDate() + difference);
  return date;
};

const getAllByFilter = async (req, res) => {
  const { current, pageSize } = req.query;

  try {
    const count = await ReportLog.count();
    console.log(count);
    const reportLogList = await ReportLog.aggregate([
      {
        $sort: {
          run_date: -1,
        },
      },
      {
        $lookup: {
          from: "reports",
          localField: "report_id",
          foreignField: "_id",
          as: "report",
        },
      },
      { $unwind: "$report" },
      {
        $lookup: {
          from: "users",
          localField: "user_id",
          foreignField: "_id",
          as: "user",
        },
      },
      { $unwind: "$user" },
      {
        $project: {
          _id: 1, // Optional: remove Mongo's default _id field from the result
          reportName: "$report.name",
          sleeperId: "$user.sleeper_id",
          userName: "$user.name",
          isRun: "$is_run",
          isSchedule: "$is_schedule",
          runDate: "$run_date",
          scheduleDate: "$schedule_date",
          recipient: "$recipient",
          content: 1,
        },
      },
      { $skip: (parseInt(current) - 1) * parseInt(pageSize) },
      {
        $limit:
          parseInt(pageSize) > count.length ? count.length : parseInt(pageSize),
      },
    ]);
    console.log(reportLogList);

    if (reportLogList) {
      res.status(Success).json({ list: reportLogList, total: count });
    } else {
      res
        .status(NotFound)
        .json({ list: reportLogList, message: "Reports not found." });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error." });
  }
};

const getAllByUser = async (req, res) => {
  try {
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { sleeper_id, id } = decoded;

    const reportLogList = await ReportLog.aggregate([
      {
        $match: {
          user_id: new mongoose.Types.ObjectId(id),
        },
      },
      {
        $lookup: {
          from: "reports",
          localField: "report_id",
          foreignField: "_id",
          as: "report",
        },
      },
      { $unwind: "$report" },
      {
        $project: {
          _id: 1, // Optional: remove Mongo's default _id field from the result
          report_name: "$report.name",
          content: 1,
          run_date: 1,
          is_file: 1,
        },
      },
      {
        $sort: {
          run_date: -1,
        },
      },
    ]);

    if (reportLogList) {
      res.status(Success).json({ list: reportLogList });
    } else {
      res
        .status(NotFound)
        .json({ list: reportLogList, message: "ReportLogs not found." });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const getAllByReport = async (req, res) => {
  try {
    console.log("byreport", req.query);
    const { league_id } = req.query;
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { sleeper_id, id } = decoded;
    console.log(decoded);
    const reportLogList = await ReportLog.aggregate([
      {
        $match: {
          user_id: new mongoose.Types.ObjectId(id),
          league_id: league_id,
        },
      },
      {
        $sort: {
          run_date: -1,
        },
      },
      {
        $lookup: {
          from: "reports",
          localField: "report_id",
          foreignField: "_id",
          as: "report",
        },
      },
      { $unwind: "$report" },
      {
        $group: {
          _id: {
            report_id: "$report_id",
            report_name: "$report.name",
            priority: "$report.priority",
          },
          latestRunDate: { $first: "$run_date" },
          content: { $first: "$content" },
        },
      },
      {
        $project: {
          report_id: "$_id.report_id",
          report_name: "$_id.report_name",
          priority: "$_id.priority",
          latestRunDate: 1,
          content: 1,
          _id: 0,
        },
      },
      {
        $sort: {
          priority: 1,
        },
      },
    ]);

    const user = await User.findById(new mongoose.Types.ObjectId(id));
    const reportSchedule = user?.report_schedule;

    let newList = [];
    console.log(reportLogList.length);

    reportLogList.map((item) => {
      let nextScheduleDate = null;
      const runDate = new Date(item.latestRunDate);
      let dayOfWeek = runDate.getDay();
      if (
        !isEmpty(reportSchedule) &&
        !isEmpty(reportSchedule[item.report_id])
      ) {
        nextScheduleDate = findNextDay(
          reportSchedule[item.report_id],
          dayOfWeek,
          runDate
        );
      }

      newList.push({ ...item, nextScheduleDate });
    });

    if (reportLogList) {
      res.status(Success).json({ list: newList });
    } else {
      res.status(NotFound).json({ list: [], message: "ReportLogs not found." });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const addReport = async (req, res) => {
  try {
    const token = req.headers["x-auth-token"];
    const decoded = JWT.decode(token);
    const { sleeper_id, id } = decoded;

    const newReport = await Report.create(req.body);

    if (!newReport) {
      return res.status(ServerError).json({ message: "Server error" });
    }

    return res.status(Created).json({ message: "Report created successfully" });
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: "Server error" });
  }
};

const deleteReport = async (req, res) => {
  try {
    const { id } = req.params;
    const report = await ReportLog.findByIdAndDelete(id);

    if (!report) {
      return res.status(404).json({ error: "No report found with this ID" });
    }

    return res
      .status(200)
      .json({ message: "Report successfully deleted", data: report });
  } catch (error) {
    console.error("Error occurred while deleting report", error);
    return res
      .status(500)
      .json({ error: "An error occurred while deleting report" });
  }
};

module.exports = {
  getAllByFilter,
  getAllByUser,
  getAllByReport,
  addReport,
  deleteReport,
};
