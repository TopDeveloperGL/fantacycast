const User = require("../models/User.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../config/keys");

const {
  ServerError,
  Conflict,
  Created,
  NotFound,
  Success,
  Invalid,
} = require("../constants/StatusCode");
const {
  ConflictMsg,
  SUCCESSMsg,
  ServerErrorMsg,
  CreatedMsg,
  NotFoundMsg,
  InvalidMsg,
} = require("../constants/StatusMessage");
const { ObjectId } = require("mongodb");

const loginAdmin = async (req, res) => {
  const { email, password } = req.body;
  console.log(req.body);
  try {
    const user = await User.findOne({ email, is_admin: 1 });

    if (!user) {
      return res.status(NotFound).json({ message: NotFoundMsg });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(Invalid).json({ message: InvalidMsg });
    }

    const token = jwt.sign(
      { id: user._id, is_admin: 1 },
      process.env.SECRET_OR_KEY,
      {
        expiresIn: 86400, // 24 hours
      }
    );

    return res.status(Success).json({ success: true, token });
  } catch (error) {
    return res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const admin_register = (req, res) => {};

const change_password = async (req, res) => {
  const { password } = req.body;
  const token = req.headers["x-auth-token"];

  try {
    const decoded = jwt.verify(token, process.env.SECRET_OR_KEY);

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const user = await User.findByIdAndUpdate(decoded.id, {
      $set: { password: hashedPassword },
    });

    if (!user) {
      return res.status(NotFound).json({ message: NotFoundMsg });
    }

    return res
      .status(Success)
      .json({ message: "Password updated successfully" });
  } catch (error) {
    if (error instanceof jwt.JsonWebTokenError) {
      return res.status(401).json({ message: "Unauthorized!" });
    }

    return res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

/**
 * Login user.
 */
const login_user = async (req, res) => {
  console.log(req.body);
  const { sleeperId, password } = req.body;

  try {
    const user = await User.findOne({ is_admin: 0, sleeper_id: sleeperId });
    console.log(user);
    if (!user) {
      return res.status(NotFound).json({ message: NotFoundMsg });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(Invalid).json({ message: InvalidMsg });
    }

    const token = jwt.sign(
      {
        sleeper_id: user.sleeper_id,
        id: user._id,
        email: user.email,
        user_role: 1,
      },
      process.env.SECRET_OR_KEY,
      {
        expiresIn: 86400, // 24 hours
      }
    );

    return res.status(Success).json({ success: true, token, role: 1 });
  } catch (error) {
    return res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

/**
 * Register user.
 */
const register_user = async (req, res) => {
  console.log(req.body);
  const { sleeperId, password, name, email } = req.body;

  try {
    const existingUser = await User.findOne({ sleeper_id: sleeperId });

    if (existingUser) {
      return res.status(Conflict).json({ message: ConflictMsg });
    }

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);

    const newUser = new User({
      sleeper_id: sleeperId,
      name,
      email,
      password: hashedPassword,
    });

    const savedUser = await newUser.save();

    if (!savedUser) {
      return res.status(ServerError).json({ message: ServerErrorMsg });
    }

    return res.status(Created).json({ message: CreatedMsg });
  } catch (error) {
    return res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const verify_code = (req, res) => {};

module.exports = {
  loginAdmin,
  admin_register,
  login_user,
  register_user,
  verify_code,
  change_password,
};
