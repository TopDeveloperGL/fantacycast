const voice = require("elevenlabs-node");
const fs = require("fs");
const path = require("path");
const sgMail = require("@sendgrid/mail");
const ReportLog = require("../models/ReportLog.model");
const isEmpty = require("is-empty");

const generate_audio = (req, res) => {
  console.log(req.body);
  const apiKey = process.env.ELEVENLABS_API_KEY; // Your API key from Elevenlabs
  const voiceID = req.body.voiceID; // The ID of the voice you want to get
  const fileName = `${path.dirname(__dirname)}/public/audio/audio.mp3`; // The name of your audio file
  const textInput = req.body.text; // The text you wish to convert to speech

  voice
    .textToSpeech(
      apiKey,
      voiceID,
      fileName,
      textInput
      // req.body.stability,
      // req.body.similarityBoost
    )
    .then((response) => {
      if (!isEmpty(response) && response.status === "ok") res.send("Success");
      else res.send("Fail");
    });
};

const generateFile = async (voiceId, text, reportlog_id) => {
  try {
    const fileName = `${path.dirname(
      __dirname
    )}/public/audio/${reportlog_id}.mp3`; // The name of your audio file
    console.log(`${process.env.ELEVENLABS_API_KEY}`, voiceId, fileName, text);
    let result = await voice.textToSpeech(
      `${process.env.ELEVENLABS_API_KEY}`,
      voiceId,
      fileName,
      text
      // req.body.stability,
      // req.body.similarityBoost
    );
    if (result.status === "ok") {
      await ReportLog.findByIdAndUpdate(reportlog_id, {
        $set: { is_file: true },
      });
    }
    // sendEmail(reportlog_id, recipient, report_name, text);
  } catch (err) {
    console.log(err);
  }
};

const sendEmail = async (req, res) => {
  sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);
  const { fileID, recipient, report_name, content } = req.body;
  try {
    const fileName = `${path.dirname(__dirname)}/public/audio/${fileID}.mp3`;
    const encodedFile = fs.readFileSync(fileName).toString("base64");
    await Promise.all(
      recipient.map((recipient_mail) => {
        let mail = {
          to: `${recipient_mail}`,
          from: `${process.env.SENDER_EMAIL}`,
          subject: "Fantasy football",
          text: "FantasyCast",

          html: `
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                body {
                    color: #1a1a1a;
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 0;
                }

                .container {
                    width: 80%;
                    margin: auto;
                    background-color: #f2f2f2;
                    padding: 20px;
                    border-radius: 15px;
                }

                .header, .content {
                    text-align: center;
                }

                .header {
                    margin-bottom: 50px;
                    padding-top: 30px;
                }

                .header h1 {
                    margin: 0;
                    color: #4d4d4d;
                }

                .header p {
                    color: #999;
                }

                .content {
                    padding-bottom: 30px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <div class="header">
                    <h1>Welcome to Our Newsletter!</h1>
                    <p>Thank you for subscribing.</p>
                </div>
                <div class="content">
                    <h2>${report_name}</h2>
                    <p>${content}</p>
                </div>
            </div>
        </body>
        </html>`,
          attachments: [
            {
              content: encodedFile,
              filename: "audio.mp3",
              type: "audio/mpeg",
              disposition: "attachment",
            },
          ],
        };

        sgMail
          .send(mail)
          .then(() => console.log(`${recipient_mail} send successfully`))
          .catch((error) => console.error(error));
      })
    );
    res.send("Email send");
  } catch (err) {
    console.log(err);
  }
};

const scheduleEmail = async (req) => {
  sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);
  const { fileID, recipient, report_name, content } = req;
  try {
    const fileName = `${path.dirname(__dirname)}/public/audio/${fileID}.mp3`;
    const encodedFile = fs.readFileSync(fileName).toString("base64");
    await Promise.all(
      recipient.map((recipient_mail) => {
        let mail = {
          to: `${recipient_mail}`,
          from: `${process.env.SENDER_EMAIL}`,
          subject: "Fantasy football",
          text: "FantasyCast",

          html: `
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                body {
                    color: #1a1a1a;
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 0;
                }

                .container {
                    width: 80%;
                    margin: auto;
                    background-color: #f2f2f2;
                    padding: 20px;
                    border-radius: 15px;
                }

                .header, .content {
                    text-align: center;
                }

                .header {
                    margin-bottom: 50px;
                    padding-top: 30px;
                }

                .header h1 {
                    margin: 0;
                    color: #4d4d4d;
                }

                .header p {
                    color: #999;
                }

                .content {
                    padding-bottom: 30px;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <div class="header">
                    <h1>Welcome to Our Newsletter!</h1>
                    <p>Thank you for subscribing.</p>
                </div>
                <div class="content">
                    <h2>${report_name}</h2>
                    <p>${content}</p>
                </div>
            </div>
        </body>
        </html>`,
          attachments: [
            {
              content: encodedFile,
              filename: "audio.mp3",
              type: "audio/mpeg",
              disposition: "attachment",
            },
          ],
        };

        sgMail
          .send(mail)
          .then(() => console.log(`${recipient_mail} send successfully`))
          .catch((error) => console.error(error));
      })
    );
    return "Email send";
  } catch (err) {
    console.log(err);
  }
};

const download_audio = (req, res) => {
  // const fileName = "audio.mp3"; // The name of your audio file
  // let url = __dirname;
  // url = url.replace("controllers", `${fileName}`);
  // const file = url;
  const filePath = `${path.dirname(__dirname)}/public/audio/${
    req.params.id
  }.mp3`;

  res.download(filePath); // Set disposition and send it.
};

const sendAudioFile = (req, res) => {
  const filePath = `${path.dirname(__dirname)}/public/audio/${
    req.params.id
  }.mp3`;

  res.sendFile(filePath); // Set disposition and send it.
};

const get_voice_list = async (req, res) => {
  const apiKey = process.env.ELEVENLABS_API_KEY; // Your API key from Elevenlabs
  let voiceList = await voice.getVoices(apiKey);

  if (voiceList) {
    res.send(voiceList.voices);
  } else {
    res.send([]);
  }
};

const sendFile = (req, res) => {
  sgMail.setApiKey(`${process.env.SENDGRID_API_KEY}`);
  const encodedFile = fs
    .readFileSync(
      `${path.dirname(__dirname)}/public/audio/64f7e944cdda887197edf013.mp3`
    )
    .toString("base64");

  let mail = {
    to: "superdeveloper@skiff.com",
    from: "apps@ardentprinciples.com",
    subject: "Hello world",
    text: "Hello plain world!",
    html: "<p>Hello HTML world!</p>",
    attachments: [
      {
        content: encodedFile,
        filename: "audio.mp3",
        type: "audio/mpeg",
        disposition: "attachment",
      },
    ],
  };

  sgMail
    .send(mail)
    .then(() => console.log("Email sent successfully"))
    .catch((error) => console.error(error));
};

module.exports = {
  generate_audio,
  download_audio,
  get_voice_list,
  sendFile,
  generateFile,
  sendAudioFile,
  sendEmail,
  scheduleEmail,
};
