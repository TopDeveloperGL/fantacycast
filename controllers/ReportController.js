const Report = require("../models/Report.model");

const {
  ServerError,
  Conflict,
  Created,
  NotFound,
  Success,
  Invalid,
} = require("../constants/StatusCode");
const {
  ConflictMsg,
  SUCCESSMsg,
  ServerErrorMsg,
  CreatedMsg,
  NotFoundMsg,
  InvalidMsg,
} = require("../constants/StatusMessage");
const mongoose = require("mongoose");

/**
 * Get all reports data
 */
const getAllByFilter = async (req, res) => {
  try {
    const reportList = await Report.find().sort("priority");
    console.log("generate", reportList.length);
    if (reportList) {
      res.status(Success).json({ list: reportList, total: reportList.length });
    } else {
      res.status(NotFound).json({
        list: reportList,
        total: reportList.length,
        message: "Reports not found.",
      });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const getAllWithLog = async (req, res) => {
  try {
    const reportList = await Report.aggregate([
      {
        $lookup: {
          from: "report_logs",
          let: { report_id: "$_id" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ["$report_id", "$$report_id"],
                },
              },
            },
            {
              $sort: {
                date: -1,
              },
            },
          ],
          as: "report_logs",
        },
      },
      {
        $unwind: "$report_logs",
      },
      {
        $group: {
          _id: 1,
          // name: 1,
          report_logs: {
            $first: "$report_logs",
          },
        },
      },
    ]);

    if (reportList) {
      res.status(Success).json({ list: reportList });
    } else {
      res
        .status(NotFound)
        .json({ list: reportList, message: "Reports not found." });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const addReport = async (req, res) => {
  const { name, description, priority } = req.body;
  console.log(req.body);
  const schedule_list = [];
  try {
    const newReport = new Report({
      name,
      description,
      schedule_list,
    });

    const savedReport = await newReport.save();

    if (!savedReport) {
      return res.status(ServerError).json({ message: ServerErrorMsg });
    }

    return res.status(Created).json({ message: CreatedMsg });
  } catch (error) {
    return res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const updatePrio = async (req, res) => {
  try {
    console.log(req.body);
    const { _id, priority } = req.body;

    const report = await Report.findById(new mongoose.Types.ObjectId(_id));
    console.log(typeof _id);

    const updatedReport = await Report.findByIdAndUpdate(_id, req.body, {
      new: true,
    });
    if (updatedReport) {
      res
        .status(Success)
        .json({ report: updatedReport, message: "Update successful" });
    } else {
      res.status(NotFound).json({ message: "Report not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const updateReport = async (req, res) => {
  try {
    console.log(req.body);
    const { _id, name, description, schedule_list } = req.body;

    const report = await Report.findById(new mongoose.Types.ObjectId(_id));
    console.log(typeof _id);
    if (typeof _id === "number") {
      let savedReport = Report.create({ name, description, schedule_list });
      if (savedReport) {
        res
          .status(Success)
          .json({ report: savedReport, message: "Saved successful" });
      } else {
        res.status(NotFound).json({ message: "Report not found" });
      }
    } else {
      const updatedReport = await Report.findByIdAndUpdate(_id, req.body, {
        new: true,
      });
      if (updatedReport) {
        res
          .status(Success)
          .json({ report: updatedReport, message: "Update successful" });
      } else {
        res.status(NotFound).json({ message: "Report not found" });
      }
    }
  } catch (error) {
    console.error(error);
    res.status(ServerError).json({ message: ServerErrorMsg });
  }
};

const deleteReport = async (req, res) => {
  try {
    const { id } = req.params;
    const report = await Report.findByIdAndDelete(id);

    if (!report) {
      return res.status(404).json({ error: "No report found with this ID" });
    }

    return res
      .status(200)
      .json({ message: "Report successfully deleted", data: report });
  } catch (error) {
    console.error("Error occurred while deleting report", error);
    return res
      .status(500)
      .json({ error: "An error occurred while deleting report" });
  }
};
module.exports = {
  getAllByFilter,
  getAllWithLog,
  addReport,
  updateReport,
  deleteReport,
  updatePrio,
};
