const { ChatOpenAI } = require("langchain/chat_models/openai");
const {
  SystemMessagePromptTemplate,
  HumanMessagePromptTemplate,
  ChatPromptTemplate,
} = require("langchain/prompts");
// const env = require("dotenv").config();
const { LLMChain } = require("langchain/chains");

const getCompletion = async (req, res) => {
  console.log(req.body);
  let result = await generateCompletion(req.body.text);
  res.send(result);
};

const generateCompletion = async (text) => {
  const chat = new ChatOpenAI({
    modelName: "gpt-4",
    temperature: 0.5,
    maxTokens: 1500,
    // streaming: true,
    openAIApiKey: process.env.YOUR_API_KEY, // In Node.js defaults to process.env.OPENAI_API_KEY
  });

  const reportPrompt = ChatPromptTemplate.fromPromptMessages([
    SystemMessagePromptTemplate.fromTemplate(`You are a helpful assistant.`),
    HumanMessagePromptTemplate.fromTemplate("{text}"),
  ]);

  const chain = new LLMChain({
    prompt: reportPrompt,
    llm: chat,
  });

  const response = await chain.call({ text: text });

  return response.text;
};

module.exports = {
  getCompletion,
  generateCompletion,
};
