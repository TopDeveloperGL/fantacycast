const cron = require("node-cron");
const ReportLog = require("../models/ReportLog.model");
const Report = require("../models/Report.model");
const User = require("../models/User.model");
const Schedule = require("../models/Schedule.model");
const League = require("../models/Leauge.model");
const mongoose = require("mongoose");
const {
  generatePreMatchupAnalysis,
  generatePostMatchupAnalysis,
  generatePowerRanking,
  generateTeamNewsAnalysis,
} = require("../controllers/OldPromptController");
const { calculateWeeks, getNextMonday } = require("../utils/report");
const isEmpty = require("is-empty");
const sleeper_package = require("sleeper_fantasy");
const audioController = require("../controllers/audioController");
const { scheduleEmail } = require("../controllers/audioController");
const days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const waitTime = (time = 100) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

cron.schedule("25 39 19 * * 0-6", async () => {
  console.log("ddd");
  let userData = await User.find({ is_admin: 0 });
  let reportData = await Report.find({});
  let scheduleData = await Schedule.find({});
  // let data = {
  //   voiceId: voiceID,
  //       weeks: weeks,
  //       week: week,
  //       matchup: matchup,
  //       team: team,
  //       league: league.leagueId,
  //       reportId: selectReport._id,
  //       recipient: emails
  // }
  let season = new Date().getFullYear();
  console.log(typeof season);
  let leagueData = await League.find({ season: season.toString() });
  let day = new Date().getDay();
  let week = 1;
  let nextMonday = getNextMonday(`${process.env.START_DATE}`);
  let todayDay = new Date().toISOString().split("T")[0];
  console.log(new Date(nextMonday).getTime(), new Date(todayDay).getTime());
  if (new Date(nextMonday).getTime() <= new Date(todayDay).getTime()) {
    week = calculateWeeks(
      getNextMonday(`${process.env.START_DATE}`),
      new Date().toISOString().split("T")[0]
    );
  }

  // const league_ids = [req.body.league];
  // sleeper_instance.leagues = league_ids;
  // let fetchleagues = await Promise.all(sleeper_instance.league_promises);

  // let season = new Date().getFullYear();
  // let leagueData = fetchleagues[0];

  // console.log(leagueData);

  // await Promise.all(
  scheduleData.map((schedule) => {
    console.log(userData);
    let user = {};
    userData.forEach((item) => {
      if (item["_id"] === schedule["user_id"]) {
        user = item;
      }
    });
    console.log(user);
    let scheduleByUser = schedule?.report_schedule;
    let voiceUser = schedule?.report_voice;
    let recipient = [user.email];
    let decoded = {
      sleeper_id: user.sleeper_id,
      id: user._id,
      user_role: 1,
    };
    console.log(scheduleByUser);
    console.log(voiceUser);

    // if (!isEmpty(scheduleByUser)) {
    //   await Promise.all(
    //     reportData.map(async (report) => {
    //       if (
    //         // !isEmpty(scheduleByUser[report._id]) &&
    //         !isEmpty(voiceUser[report._id])
    //         // scheduleByUser[report._id].includes(day) &&
    //         // report.schedule_list.includes(days[day])
    //       ) {
    //         let req = {
    //           league: schedule.league_id,
    //           week: 0,
    //           team: 0,
    //           weeks: [],
    //           matchup: 0,
    //           reportId: report._id,
    //           recipient: recipient,
    //           voiceId: voiceUser[report._id],
    //           decoded: decoded,
    //         };
    //         console.log(report._id, voiceUser[report._id], recipient);
    //         let result;
    //         if (report.name === "Pre-Matchup Analysis Report") {
    //           result = await generatePreMatchupAnalysis(req);
    //         }
    //         if (report.name === "Team News Analysis Report") {
    //           result = await generateTeamNewsAnalysis(req);
    //         }
    //         if (report.name === "Power Rankings Report") {
    //           result = await generatePowerRanking(req);
    //         }
    //         if (report.name === "Post-Matchup Analysis Report") {
    //           result = await generatePostMatchupAnalysis(req);
    //         }
    //         await audioController.generateFile(
    //           voiceUser[report._id],
    //           result.content,
    //           result.id
    //         );
    //         await waitTime(90000);
    //         await scheduleEmail({
    //           fileID: result.id,
    //           recipient: recipient,
    //           report_name: result.name,
    //           content: result.content,
    //         });
    //       }
    //     })
    //   );
    // }
  });
  // );
});
// const autoSendEmail = async () => {
//   // Schedule the Pre-Matchup Analysis Report to run on Tue, Wed, Thu, Fri, Sat, Sun at 9 AM
//   cron.schedule("0 9 * * 2-7", async () => {
//     console.log("Generating Pre-Matchup Analysis Report...");
//     // Add your Pre-Matchup Analysis Report generation logic here
//     try {
//       // Add your report creation logic here
//       const newReport = await ReportLog.findByIdAndUpdate();
//       console.log("Report added:", newReport);
//     } catch (error) {
//       console.error("Error adding report:", error);
//     }
//   });

//   // Schedule the Post Matchup Analysis Report to run on Tue, Wed at 9 AM
//   cron.schedule("0 9 * * 2-3", async () => {
//     console.log("Generating Post Matchup Analysis Report...");
//     // Add your Post Matchup Analysis Report generation logic here
//   });

//   // Schedule the Power Rankings Report to run every day at 9 AM
//   cron.schedule("0 9 * * *", async () => {
//     console.log("Generating Power Rankings Report...");
//     // Add your Power Rankings Report generation logic here
//   });

//   // Schedule the Team Analysis Report to run every day at 9 AM
//   cron.schedule("0 9 * * *", async () => {
//     console.log("Generating Team Analysis Report...");
//     // Add your Team Analysis Report generation logic here
//   });

//   // Schedule the Transaction Analysis Report to run when a trade happens
//   // Add your logic to trigger the report generation when a trade occurs

//   // Schedule the Team News Analysis Report to run every day at 9 AM
//   cron.schedule("0 9 * * *", async () => {
//     console.log("Generating Team News Analysis Report...");
//     // Add your Team News Analysis Report generation logic here
//   });
// };

// module.exports = autoSendEmail;
